/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package nunchuck;

import java.io.IOException;
import java.util.Stack;

import serial.ModConnector;
import ui.NunchuckPanel;
import util.Configuration;
import util.StackTraceUtil;

import com.centralnexus.input.*;

/**
 * ROVJoyStick test class - CentralNexus impl
 * @param
 * @implements JoystickListener, Runnable
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public class CopyOfROVJoyStick extends Thread implements JoystickListener {

	private static CopyOfROVJoyStick R;
	private NunchuckPanel np;
	private final static double EPSILON = 0.00001;
	
	byte[] data;
	Joystick js;
	
	int x;
	int y;
	boolean btn_c;
	boolean btn_z;
	Stack<Integer> stack_x = new Stack<Integer>();
	Stack<Integer> stack_y = new Stack<Integer>();
	private final int c = 1;
	private final int z = 2;
	private String btnState;
	
	public static synchronized CopyOfROVJoyStick getInstance() {
		
		if( R == null)
			return new CopyOfROVJoyStick();
		return R;
	}
	
	private CopyOfROVJoyStick() {
		
		this.x = 127;
		this.y = 127;
		
		try {
			
			js = Joystick.createInstance();
			js.addJoystickListener(this);
			//js.setDeadZone(0.001);		
			this.start();
			np = NunchuckPanel.getInstance();
			updateTop("Joystick Connected");
			
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Please connect your nunchuck!");			
		}		
	}
	
	@Override
	public void joystickAxisChanged(Joystick arg0) {
		
		updateTop("Axis changed");
		// Convert to 0-255
		//int hold_x = 
		//this.x = (int) ((js.getX() * ( 128 - EPSILON)));
		//this.x = hold_x + 127;
		//this.x = (int) (js.getX() * 100);
		//int hold_y = (int) ((js.getY() * ( 128 - EPSILON))); 
		//this.y = (int) ((js.getY() * ( 128 - EPSILON)));
		//this.y = hold_y + 127;
		//this.y = (int) (js.getY() * 100);

		updateTop("Coordinates Converted");

		// Catch any anomalies
		/*
		if(this.x < 0) this.x = 0;
		if(this.y > 255) this.y = 255;
		if(this.x < 0) this.x = 0;
		if(this.y > 255) this.y = 255;
		updateTop("Anomalies detected");
		*/

		// Detect zero range
		/*
		if (this.x < 134 && this.x > 117)
			this.x = 127;
		if(this.y < 134 && this.y > 120)
			this.y = 127;
		updateTop("Zeros Handled");
		*/
		
		//If !deadzone, unblock
		if(isValid()) {
			synchronized(this) {
				System.out.println("Axis Change Interrupt");
				notify();
			}
		}
	}
	
	private boolean isValid() {
		/*
		stack_x.push(this.x);
		stack_y.push(this.y);

		//Check if duplicate
		if(((Math.abs(stack_x.pop() - stack_x.peek())) == 0) &&
				(Math.abs(Math.abs(stack_x.pop() - stack_y.peek())) == 0))
				return false;*/
		return true;
	}

	@Override
	public void joystickButtonChanged(Joystick arg0) {
		
		//Invert the control buttons on the nunchuck
		if(!Configuration.IS_INVERTED) {
			
			this.btn_c = js.isButtonDown(c);
			this.btn_z = js.isButtonDown(z);
			
		}
		else {
			
			this.btn_c = !(js.isButtonDown(c));
			this.btn_z = !(js.isButtonDown(z));
		}
		
		synchronized(this) {
			System.out.println("Button action Interrupt");
			notify();
		}
	}
	
	public int getXcord() {
		return this.x;
	}
	
	public int getYcord() {
		return this.y;
	}
	
	public boolean getCbtn() {
		return this.btn_c;
	}
	
	public boolean getZbtn() {
		return this.btn_z;
	}
	
	public static void main(String[] args) {
		CopyOfROVJoyStick j = new CopyOfROVJoyStick();
		j.run();	
	}

	@Override
	public void run() {
		
		while(true) {
			
			synchronized(this) {
					try {
						if(this.isAlive())
							wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				
				try {
					wait();				
					btnState  = (this.btn_c) ? " C active ::" : "";
					btnState += (this.btn_z) ? " Z active " : "";
					updateTop("X: " + this.x + " Y: " + this.y + btnState);
					System.out.println(this.x + " " + this.y);
					sendData();
					notify();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public void sendData() {
		
		data = new byte[6];
		
		// First we'll need to read the first register so 
		// We can restore the existing values
		try {
			
			//TODO
			//Attempt to create ROVSerialInterface
			//TEST_ModCon.getInstance().setAction(0, 1, 1);
			//byte hold = Byte.parseByte(TEST_ModCon.getInstance().getReadResponse());
			//System.out.print(hold);

	
		// Build byte array to send to Modbus
		// Are both buttons on?
		// ... just C?
		// ....... just z?
		data[0] = 0;
		data[1] = 0;
		/*
				(byte) (
				((this.btn_c) && (this.btn_z)) ? 3 :
					(this.btn_c) ? 2 : 
						(this.btn_z) ? 1 :0
						);*/
							
		
		// Y-axis upper bits
		data[2] = 0;
		
		// X-axis upper bits... will always be zero because we're < 255 for all axis
		data[3] = 127;// (byte) (this.x);
				
		// X-axis upper bits
		data[4] = 0;
	
		// Y-axis lower bits... will always be zero because we're < 255 for all axis
		data[5] = 127;// (byte) (this.y);
		

		
		for(int x=0; x<data.length; x++) 
			System.out.print(data[x] + " ");
		System.out.println();
		
		
		// All writes start at 0
		ModConnector.getInstance().setAction("writeall", 0, data);
		new Thread(new Runnable() {
			public void run(){
				try{
					ModConnector.getInstance().run();
				} catch (Exception e) {
					StackTraceUtil.getStackTrace(e);
				}
			}
		}).start();		
		
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
	}
	
	private void updateTop(String c) {
		np.updateGUI(c);
	}
}
