/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package nunchuck;

import java.io.IOException;
import java.util.Stack;

import serial.ModConnector;
import util.Configuration;
import util.StackTraceUtil;

import com.centralnexus.input.*;

/**
 * JoyStick test class - CentralNexus impl
 * @param
 * @implements JoystickListener, Runnable
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public class JoyTest extends Thread implements JoystickListener {

	private static JoyTest R;
	private final static double EPSILON = 0.00001;
	
	byte[] data;
	Joystick js;
	
	int x;
	int y;
	boolean btn_c;
	boolean btn_z;
	Stack<Integer> stack_x = new Stack<Integer>();
	Stack<Integer> stack_y = new Stack<Integer>();
	private final int c = 1;
	private final int z = 2;
	
	
	public static synchronized JoyTest getInstance() {
		
		if( R == null)
			return new JoyTest();
		return R;
	}
	
	private JoyTest() {
		
		x = 0;
		y = 0;
		
		try {
			
			js = Joystick.createInstance();
			js.addJoystickListener(this);
			//js.setDeadZone(0.001);		
			this.start();
			
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Please connect your nunchuck!");			
		}		
	}
	
	@Override
	public void joystickAxisChanged(Joystick arg0) {
		
		//If !deadzone, unblock
		if(isValid()) {
			synchronized(this) {
				System.out.println("Axis Change Interrupt");
				notify();
			}
		}
	}
	
	private boolean isValid() {
		
		this.x = (int) (js.getX() * ( 128 - EPSILON));
		this.y = (int) (js.getY() * ( 128 - EPSILON));
		stack_x.push(this.x);
		stack_y.push(this.y);
		
		//System.out.println("Old: " + x + " New: " + this.x);
		//System.out.println((Math.abs(Math.abs(this.x) - Math.abs(x))));
		
		if(Math.abs(stack_x.peek()) > 5 && Math.abs(stack_y.peek()) > 5) {
			if(((Math.abs(Math.abs(stack_x.pop()) - Math.abs(stack_x.peek()))) <= 1) &&
			   ((Math.abs(Math.abs(stack_x.pop()) - Math.abs(stack_y.peek()))) <= 1))
				return false;
			return true;

		}
		return false;
	}

	@Override
	public void joystickButtonChanged(Joystick arg0) {
		
		//Invert the control buttons on the nunchuck
		if(!Configuration.IS_INVERTED) {
			
			this.btn_c = js.isButtonDown(c);
			this.btn_z = js.isButtonDown(z);
			
		}
		else {
			
			this.btn_c = !(js.isButtonDown(c));
			this.btn_z = !(js.isButtonDown(z));
		}
		
		synchronized(this) {
			System.out.println("Button action Interrupt");
			notify();
		}
	}
	
	public int getXcord() {
		return this.x;
	}
	
	public int getYcord() {
		return this.y;
	}
	
	public boolean getCbtn() {
		return this.btn_c;
	}
	
	public boolean getZbtn() {
		return this.btn_z;
	}
	
	public static void main(String[] args) {
		JoyTest j = new JoyTest();
		j.run();	
	}

	@Override
	public void run() {
		
		while(true) {
			
			synchronized(this) {
					try {
						if(this.isAlive())
							wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				
				try {
					wait();
					System.out.println(this.x + " " + this.y);
					sendData();
					notify();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public void sendData() {
		
		data = new byte[6];
		
		// First we'll need to read the first register so 
		// We can restore the existing values
		try {
			
			//TODO
			//Attempt to create ROVSerialInterface
			//TEST_ModCon.getInstance().setAction(0, 1, 1);
			//byte hold = Byte.parseByte(TEST_ModCon.getInstance().getReadResponse());
			//System.out.print(hold);

	
		// Build byte array to send to Modbus
		// Are both buttons on?
		// ... just C?
		// ....... just z?
		data[0] = (byte) (
				((this.btn_c) && (this.btn_z)) ? 3 :
					(this.btn_c) ? 2 : 
						(this.btn_z) ? 1 :0
						);
		
		//data[0] &= hold;
		
		
		// This cannot be null
		data[1] = 0;
		
		// X-axis upper bits... will always be zero because we're < 255 for all axis
		data[2] = (byte) (((this.x < 0) ? this.x * -2 : this.x * 2));// > 127 ? 128 : ((this.x < 0) ? this.x * -2 : this.x * 2));
		
		// X-axis lower bits
		data[3] = 0;
	
		// Y-axis upper bits... will always be zero because we're < 255 for all axis
		data[4] = (byte) (((this.y < 0) ? this.y * -2 : this.y * 2));// > 127 ? 128 : ((this.y < 0) ? this.y * -2 : this.y * 2));
		
		// Y-axis lower bits
		data[5] = 0;
		
		
		for(int x=0; x<data.length; x++) 
			System.out.print(data[x] + " ");
		System.out.println();
		
		
		// All writes start at 0
		ModConnector.getInstance().setAction("writeall", 0, data);
		new Thread(new Runnable() {
			public void run(){
				try{
					ModConnector.getInstance().run();
				} catch (Exception e) {
					StackTraceUtil.getStackTrace(e);
				}
			}
		}).start();		
		
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
	}
}
