/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package nunchuck;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import serial.ModConnector;
import ui.ControlPanel;
import ui.NunchuckPanel;
import util.Configuration;
import util.StackTraceUtil;

import com.centralnexus.input.*;

/**
 * ROVJoyStick test class - CentralNexus impl
 * 
 * @param
 * @implements JoystickListener, Runnable
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public class ROVJoyStick extends Thread implements JoystickListener {

	private static ROVJoyStick R;
	private static final Logger log = Logger.getLogger(ROVJoyStick.class.getPackage().getName());

	private NunchuckPanel np;
	private final static double EPSILON = 0.00001;
	
	byte[] data;
	Joystick js;
	
	// X and Y coordinates are flipped!!
	// Initialize to zero (0-255, 127 is center)
	int x = 127;
	int y = 127;
	boolean btn_c;
	boolean btn_z;
	private final int c = 1;
	private final int z = 2;
	private String btnState;
	private int zCount;
	
	public static synchronized ROVJoyStick getInstance() {
		
		if( R == null)
			return new ROVJoyStick();
		return R;
	}
	
	private ROVJoyStick() {
				
		try {
			Handler fh;
			fh = new FileHandler("%t/MarineElectric.log");
			log.addHandler(fh);
			
			zCount = 0;
			np = NunchuckPanel.getInstance();
			updateTop("Initiating Joystick...");
			
			// Allow serial port to initialize
			Thread.sleep(2800);
			js = Joystick.createInstance();
			js.addJoystickListener(this);	
			
			this.start();
			updateTop("Joystick Connected");
			
		} catch (IOException e) {
			//e.printStackTrace();
			updateTop("Please connect your nunchuck!");			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	@Override
	public void joystickAxisChanged(Joystick arg0) {
		
		
		// The getAxis commands are inverted...
		//this.x = (int) (js.getY() * (180 - EPSILON));
		//this.y = (int) (js.getX() * (180 - EPSILON));

		// If !deadzone, unblock
		if(isValid()) {
			synchronized(this) {
				log.log(Level.INFO, "Axis Change Interrupt");
				notify();
			}
		}
	}
	
	
	// SWAP THE VALUES OF X AND Y TO COMPENSATE FOR BACKWARDS CONTROLS
	private synchronized boolean isValid() {
		
		// Weird floating precision error if I try to add 127 immediately...
		this.x = new Integer((int) (js.getY() * (180 - EPSILON)));
		this.x += 127;
		this.y = new Integer((int) (js.getX() * (180 - EPSILON)));
		this.y += 127;
		
		
		if(this.x < 0) this.x = 0;
		if(this.x > 255) this.x = 255;
		
		if(this.y < 0) this.y = 0;
		if(this.y > 255) this.y = 255;
		
		// -10, +5 Deadzone. The low side needs to be -10 because 
		//  The float conversion sags below 127 likely due to calibration 
		//  Disparities....
		if(this.x >= 117 && this.x <= 132) this.x = 127;
		if(this.y >= 117 && this.y <= 132) this.y = 127;
		
		return true;
	}

	@Override
	public void joystickButtonChanged(Joystick arg0) {
		
		//Invert the control buttons on the nunchuck
		if(!Configuration.IS_INVERTED) {
			
			this.btn_c = js.isButtonDown(c);
			this.btn_z = js.isButtonDown(z);
			
		}
		else {
			
			this.btn_c = !(js.isButtonDown(c));
			this.btn_z = !(js.isButtonDown(z));
		}
		
		if(this.btn_c || this.btn_z) {
			synchronized(this) {
				log.log(Level.INFO, "Button action Interrupt");
				notify();
			}
		}
	}
	
	public int getXcord() {
		return this.x;
	}
	
	public int getYcord() {
		return this.y;
	}
	
	public boolean getCbtn() {
		return this.btn_c;
	}
	
	public boolean getZbtn() {
		return this.btn_z;
	}
	
	public static void main(String[] args) {
		ROVJoyStick j = new ROVJoyStick();
		j.run();	
	}

	@Override
	public void run() {
		
		while(true) {
			
			synchronized(this) {
					try {
						if(this.isAlive())
							wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				
				try {
					wait();				
					btnState  = (this.btn_c) ? " C active ::" : "";
					btnState += (this.btn_z) ? " Z active " : "";
					updateTop("X: " + this.x + " Y: " + this.y + btnState);
					sendData();
					notifyAll();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public void sendData() {
		
		data = new byte[8];
		
		// First we'll need to read the first register so 
		// We can restore the existing values
		try {
			
			//TODO
			//Attempt to create ROVSerialInterface
			//TEST_ModCon.getInstance().setAction(0, 1, 1);
			//byte hold = Byte.parseByte(TEST_ModCon.getInstance().getReadResponse());
			//System.out.print(hold);

	
		// Build byte array to send to Modbus
		// Are both buttons on?
		// ... just C?
		// ....... just z?
		data[1] = 0x00;
		data[0] = (byte) (
				((this.btn_c) && (this.btn_z)) ? 3 :
					(this.btn_c) ? 2 : 
						(this.btn_z) ? 1 :0
						);		
		
		// Y-axis
		data[3] = 0x00;
		data[2] = (byte) (this.y);
				
		// X-axis
		data[5] = 0x00;
		data[4] = (byte) (this.x);
		
		// LED Power
		data[7] = 0x00;
		data[6] = (byte) (Configuration.LED_POWER);
		
		
		// All writes start at 0
		ModConnector.getInstance().setAction("writeall", 0, data);
		new Thread(new Runnable() {
			public void run(){
				try{
					ModConnector.getInstance().run();
				} catch (Exception e) {
					StackTraceUtil.getStackTrace(e);
				}
			}
		}).start();		
		
		} catch (Exception e) {
			log.log(Level.SEVERE, "Serios Error: ", e);
			log.log(Level.SEVERE, StackTraceUtil.getStackTrace(e));		
		}
		
	}
	
	private void updateTop(String c) {
		log.log(Level.INFO, c);
		np.updateGUI(c);
	}
}
