/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package video;

import java.awt.BorderLayout;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import ui.TextOutputPanel;
import ui.VideoPanel;
import util.Configuration;
import util.StackTraceUtil;

import com.smaxe.uv.media.core.VideoFrame;
import com.smaxe.uv.media.swing.JVideoScreen;
import com.smaxe.uv.na.webcam.IWebcam;

/**
 * RemoteCamera - JUV SMXE impl
 * Generate video frame to hold webcam stream
 * @extends Thread
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public class RemoteCamera extends Thread {

	protected static RemoteCamera R;
	
	private static final Logger log = Logger.getLogger(RemoteCamera.class.getPackage().getName());

	final AtomicReference<JFrame> frameRef = new AtomicReference<JFrame>();
	private static JFrame hostFrame;
	private static JPanel panel;
	IWebcam webcam;

	final JVideoScreen videoScreen = new JVideoScreen();
	
	public static synchronized RemoteCamera getInstance() throws Exception {
		
		if(R == null) 
			R = new RemoteCamera();
		return R;
		
	}
	
	
	private RemoteCamera() {
		panel = VideoPanel.getInstance();
		hostFrame = VideoPanel.getFrame();
	}


	public void run() {
		
		webcam = DetectCameras.getList().get(0);

		log.log(Level.INFO, "RemoteCamera launched...");
		try {
			// Set resolution low to improve performance on lower spec systems
			webcam.open(new IWebcam.FrameFormat(Configuration.VIDEO_WIDTH, Configuration.VIDEO_HEIGHT),
				new IWebcam.IListener() {
					private VideoFrame lastFrame = new VideoFrame(
							0, 0, null);

					public void onVideoFrame(
							final VideoFrame frame) {
						SwingUtilities.invokeLater(new Runnable() {
									public void run() {
										videoScreen.setFrame(frame);
										
										// Rebuild frame as needed
										if (lastFrame.width != frame.width
												|| lastFrame.height != frame.height) {
											final JFrame frame = frameRef.get();

											if (frame != null)
												frame.pack();
										}

										lastFrame = frame;
									}
								});
					}
				});
			webcam.startCapture();
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					
					frameRef.set(hostFrame);

					panel.setLayout(
							new BorderLayout());
					panel.add(videoScreen);

				}
			});
		} catch (Exception ex) {
			TextOutputPanel.getInstance().updateGUI(StackTraceUtil.getStackTrace(ex));
			log.log(Level.SEVERE, StackTraceUtil.getStackTrace(ex));
		}
	}
	
	// Quick-close for camera stream
	public void close() {
	
		webcam.close();
		
	}
	
}

