/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package video;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.AbstractAction;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.smaxe.uv.media.core.VideoFrame;
import com.smaxe.uv.media.swing.JVideoScreen;
import com.smaxe.uv.na.WebcamFactory;
import com.smaxe.uv.na.webcam.IWebcam;

/**
 * CameraTester - runs detection and streaming test
 * @extends Thread
 * 
 * @see JUV SMXE
 * 
 */
public class CameraTest extends Thread {
	public CameraTest() {
		final JComboBox webcamComboBox = new JComboBox();

		final JFrame frame = new JFrame();

		final JPanel content = new JPanel(new FlowLayout());

		content.setBorder(new EmptyBorder(8, 8, 8, 8));
		content.setPreferredSize(new Dimension(640, 480));

		content.add(new JLabel("Webcam: ", JLabel.RIGHT));
		content.add(webcamComboBox);
		content.add(new JButton(new AbstractAction("Open") {
			private final static long serialVersionUID = -4792981545160764997L;

			public void actionPerformed(ActionEvent e) {
				final IWebcam webcam = (IWebcam) webcamComboBox
						.getSelectedItem();
				
				if (webcam == null)
					return;

				final AtomicReference<JFrame> frameRef = new AtomicReference<JFrame>();

				final JVideoScreen videoScreen = new JVideoScreen();
				final AtomicBoolean videoScreenFlip = new AtomicBoolean(false);
				final AtomicBoolean videoScreenMirror = new AtomicBoolean(false);

				new Thread(new Runnable() {
					public void run() {
						try {
							webcam.open(new IWebcam.FrameFormat(320, 240),
									new IWebcam.IListener() {
										private VideoFrame lastFrame = new VideoFrame(
												0, 0, null);

										public void onVideoFrame(
												final VideoFrame frame) {
											SwingUtilities
													.invokeLater(new Runnable() {
														public void run() {
															videoScreen
																	.setFrame(frame);

															if (lastFrame.width != frame.width
																	|| lastFrame.height != frame.height) {
																final JFrame frame = frameRef
																		.get();

																if (frame != null)
																	frame.pack();
															}

															lastFrame = frame;
														}
													});
										}
									});

							webcam.startCapture();

							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									final JFrame frame = new JFrame();

									frameRef.set(frame);

									frame.getContentPane().setLayout(
											new BorderLayout());
									frame.getContentPane().add(videoScreen,
											BorderLayout.CENTER);

									frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
									frame.setResizable(false);
									frame.setTitle(webcam.getName());

									videoScreen
											.addMouseListener(new MouseAdapter() {
												@Override
												public void mouseClicked(
														final MouseEvent e) {
													final int clickCount = e
															.getClickCount();
													final Object source = e
															.getSource();

													switch (clickCount) {
													case 1: {
														if (SwingUtilities
																.isRightMouseButton(e)) {
															JPopupMenu popup = new JPopupMenu();

															popup.add(new AbstractAction(
																	"Mirror") {
																private final static long serialVersionUID = 0L;

																public void actionPerformed(
																		ActionEvent e) {
																	videoScreenMirror
																			.set(!videoScreenMirror
																					.get());
																	videoScreen
																			.mirror(videoScreenMirror
																					.get());
																}
															});

															popup.add(new AbstractAction(
																	"Flip") {
																private final static long serialVersionUID = 0L;

																public void actionPerformed(
																		ActionEvent e) {
																	videoScreenFlip
																			.set(!videoScreenFlip
																					.get());
																	videoScreen
																			.flip(videoScreenFlip
																					.get());
																}
															});

															popup.addSeparator();

															popup.add(new AbstractAction(
																	"160x120") {
																private final static long serialVersionUID = 0L;

																public void actionPerformed(
																		ActionEvent e) {
																	webcam.setFrameFormat(new IWebcam.FrameFormat(
																			160,
																			120));
																}
															});

															popup.add(new AbstractAction(
																	"320x240") {
																private final static long serialVersionUID = 0L;

																public void actionPerformed(
																		ActionEvent e) {
																	webcam.setFrameFormat(new IWebcam.FrameFormat(
																			320,
																			240));
																}
															});

															popup.add(new AbstractAction(
																	"640x480") {
																private final static long serialVersionUID = 0L;

																public void actionPerformed(
																		ActionEvent e) {
																	webcam.setFrameFormat(new IWebcam.FrameFormat(
																			640,
																			480));
																}
															});

															popup.show(
																	(Component) source,
																	e.getX(),
																	e.getY());
														}
													}
														break;
													}
												}
											});

									frame.addWindowListener(new WindowAdapter() {
										@Override
										public void windowClosing(WindowEvent e) {
											webcam.close();
										}
									});

									frame.pack();
									frame.setVisible(true);
								}
							});
						} catch (Exception ex) {
							JOptionPane.showMessageDialog(frame,
									ex.getMessage(), ex.getMessage(),
									JOptionPane.WARNING_MESSAGE);
						}
					}
				}).start();
			}
		}));

		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(content);

		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setTitle("I need a nap :-/");

		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		frame.pack();
		frame.setVisible(true);
		
		List<IWebcam> liw = WebcamFactory.getWebcams(
				frame, "avicap");
		for(IWebcam w : liw)
			System.out.println(w.getName());

		webcamComboBox.setModel(new WebcamComboModel(WebcamFactory.getWebcams(
				frame, "avicap")));

		/*
		 * webcamComboBox.setModel(new
		 * WebcamComboModel(WebcamFactory.getWebcams(frame, args.length == 0 ?
		 * "avicap" : args[0])));
		 */
	}
	
	/**
	 * <code>WebcamComboModel</code> - webcam combo model.
	 */
	private final static class WebcamComboModel extends AbstractListModel
			implements ComboBoxModel {
		private final static long serialVersionUID = -8627944517955777531L;

		// fields
		private final List<IWebcam> devices;
		// state
		private Object selected = null;

		/**
		 * Constructor.
		 * 
		 * @param devices
		 */
		public WebcamComboModel(List<IWebcam> devices) {
			this.devices = devices;
		}

		// MutableComboBoxModel implementation

		public void setSelectedItem(final Object item) {
			this.selected = item;
		}

		public Object getSelectedItem() {
			return selected;
		}

		public Object getElementAt(int index) {
			return devices.get(index);
		}

		public int getSize() {
			return devices.size();
		}
	}

	
	public static void main(String[] args) {
		CameraTest sc = new CameraTest();
		//sc.run();
		sc.start();
	}
}
