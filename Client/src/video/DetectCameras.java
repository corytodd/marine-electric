/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package video;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;

import com.smaxe.uv.na.WebcamFactory;
import com.smaxe.uv.na.webcam.IWebcam;

/**
 * DetectCameras on PC
 * Attempt to auto detect the correct webcam
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */

public class DetectCameras {
	private static final Logger log = Logger.getLogger(DetectCameras.class.getPackage().getName());

	static List<IWebcam> liw;
	
	/**
	 * getList
	 * Return a list of IWebcams attached to PC
	 * @return List<IWebcam>
	 * @see com.smaxe.uv.na.webcam.IWebcam
	 * 
	 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
	 */
	public static List<IWebcam> getList() {
		final JFrame frame = new JFrame();
		frame.pack();
		
		liw = WebcamFactory.getWebcams(
				frame, "avicap");
		for(IWebcam w : liw)
			log.log(Level.INFO, w.getName());

		return liw;
	}

}
