/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import util.Configuration;

/**
 * TextOutput panel container - All controls and stats feed
 * @param
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public class TextOutputPanel extends JPanel  {

	protected static TextOutputPanel R;
	
	private static final long serialVersionUID = 8421067318580204471L;

	private JTextArea clioutput;
    int[] i = { 0, 0, 2, 5};
    
    public static synchronized TextOutputPanel getInstance() {
		
		if(R == null) 
			R = new TextOutputPanel();
		return R;
		
    }
    
	private TextOutputPanel() {
		//Build simple text monitor 
		GridBagConstraints gbc;
		GridBagLayout gbl_monitorPanel = new GridBagLayout();
		gbl_monitorPanel.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_monitorPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_monitorPanel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_monitorPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};

		JLabel output_lbl  = new JLabel(Configuration.TEXT_OUTPUT_LABEL);
		JLabel anchor_lbl  = new JLabel(" ");
		
		this.setLayout(gbl_monitorPanel);
		gbc = GridBagFactory.getGridBag(1, 3, 0, i, 1);
		this.add(anchor_lbl, gbc);
		gbc = GridBagFactory.getGridBag(1, 4, 0, i, 0);
		this.add(output_lbl, gbc);
		gbc = GridBagFactory.getGridBag(1, 5, 0, i, 0);
		
		//The scroll panel
		final JScrollPane outputMonScroll = new JScrollPane(clioutput = new JTextArea(24, 20),
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.add(outputMonScroll, gbc);
		clioutput.setAutoscrolls(true);
		clioutput.setEditable(false);	
		updateGUI("Initiating...\n");
	}
	
	//safely update textArea
	public void updateGUI(final String m) {
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				clioutput.append(m + "\n");
			}
		});

	}
	

}
