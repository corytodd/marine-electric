/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package ui;

import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * Gridbag factory for uniform panels
 * @param
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public class GridBagFactory {
	
	public static GridBagConstraints getGridBag(int x, int y, int gridwidth, int[] i, int fill) {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridwidth = gridwidth;
		gbc.insets = new Insets(i[0], i[1], i[2], i[3]);
		gbc.fill = fill;
		gbc.gridx = x;
		gbc.gridy = y;
		
		return gbc;
	}
	
}
