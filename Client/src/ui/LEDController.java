package ui;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JLabel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import util.Configuration;

public class LEDController extends JPanel {

	private static final long serialVersionUID = 7353220229061771445L;
	private static final Logger log = Logger.getLogger(LEDController.class.getPackage().getName());

	/**
	 * Create the panel.
	 */
	public LEDController() {
		
		JLabel lblNewLabel = new JLabel("LED Power");
		add(lblNewLabel);
		
		final JSlider slider = new JSlider();
		slider.setMaximum(255);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				log.log(Level.INFO, "LED Power: " + slider.getValue());
				Configuration.setLEDPower(slider.getValue());
			}
		});
		slider.setValue(180);
		add(slider);

	}

}
