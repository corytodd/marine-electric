/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package ui;

import java.awt.GridBagLayout;

import javax.swing.JPanel;

/**
 * Config panel container
 * @param
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */

public class ConfigPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4408104100844134759L;

	public ConfigPanel() {
	    //New Gridbag
		GridBagLayout gbl_configPanel = new GridBagLayout();
		gbl_configPanel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_configPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_configPanel.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_configPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		this.setLayout(gbl_configPanel);

		ConfigGen cg = new ConfigGen();
		this.add(cg);
		
	}

}
