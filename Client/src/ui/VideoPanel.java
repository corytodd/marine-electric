/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Video panel container
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public class VideoPanel extends JPanel {
		
	protected static VideoPanel R;
	protected static JFrame parentFrame;
	
	private static final long serialVersionUID = 5621917531475647979L;

	public static synchronized VideoPanel getInstance(JFrame f) {
		
		if(R == null) 
			R = new VideoPanel(f);
		return R;
		
	}
	
	//Overload so we can grab this panel parent frame - ControlPanel
	public static synchronized VideoPanel getInstance() {
		
		assert (R != null); 
		return R;
		
	}
	
	private VideoPanel(JFrame f) {
		
		super(new BorderLayout());
		parentFrame = f;

	}
	
	public static JFrame getFrame() {
		return parentFrame;
	}
		
}
