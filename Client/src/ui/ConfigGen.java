/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package ui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import net.miginfocom.swing.MigLayout;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;

import serial.ModConnector;
import util.BaudRate;
import util.ComPort;
import util.Configuration;
import util.SerialProperties;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Provides the primary config interface
 * @param
 * @extend JPanel
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */

public class ConfigGen extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	
	/**
	 * Create the panel.
	 */
	public ConfigGen() {
		setLayout(new MigLayout("", "[80.00][grow][]", "[][][][][]"));
		
		JLabel lblNewLabel = new JLabel("Baud Rate");
		add(lblNewLabel, "cell 0 0,alignx leading");
		
		final JComboBox baud = new JComboBox();
		for(BaudRate br : BaudRate.values()) {
			baud.addItem(br.portName());
		}
		baud.setSelectedIndex(5);
		add(baud, "cell 1 0,grow");
		
		JLabel lblNewLabel_1 = new JLabel("Comm Port");
		add(lblNewLabel_1, "cell 0 1,alignx leading");
		
		final JComboBox port = new JComboBox(ComPort.values());
		port.setSelectedIndex(8);
		add(port, "cell 1 1,growx");
		
		JLabel lblNewLabel_2 = new JLabel("Encoding");
		add(lblNewLabel_2, "cell 0 2,alignx leading");
		
		final JComboBox encoding = new JComboBox();
		encoding.setModel(new DefaultComboBoxModel(new String[] {"RTU", "ASCII"}));
		encoding.setSelectedIndex(1);
		add(encoding, "cell 1 2,growx");
		
		JLabel lblNewLabel_3 = new JLabel("Invert Buttons");
		add(lblNewLabel_3, "cell 0 3,alignx leading");
		
		//Create and add checkbox
		//I wired my nunchuck buttons backwards so we'll detect and accommodate
		boolean initialInvert = System.getProperty("user.name").equalsIgnoreCase("catix") ? true : false;
		JCheckBox chckbxNewCheckBox = new JCheckBox("", initialInvert);
		add(chckbxNewCheckBox, "cell 1 3");
		Configuration.IS_INVERTED = chckbxNewCheckBox.isSelected();
		chckbxNewCheckBox.addItemListener(
			    new ItemListener() {
					@Override
					public void itemStateChanged(ItemEvent e) {
			            Configuration.IS_INVERTED = (e.getStateChange() == ItemEvent.SELECTED);
					}
			    }
			);
		
		JButton btnNewButton = new JButton("Save");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				ModConnector mt = ModConnector.getInstance();

				mt.conClose();
				
				//Set new configuration
				SerialProperties.setProperties(baud.getSelectedItem().toString(),
						port.getSelectedItem().toString(), encoding.getSelectedItem().toString());
				
				//Open a new connection
				mt = ModConnector.getInstance();
				
			}
		});
		add(btnNewButton, "cell 2 4");
	
	}

}
