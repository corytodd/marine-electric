/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

import nunchuck.ROVJoyStick;

import serial.ROVStat;
import util.Configuration;
import util.DiagnosticUnit;
import util.SerialProperties;
import util.StackTraceUtil;
import video.RemoteCamera;

/**
 * Provides the primary control interface
 * @param
 * @extend JPanel
 * @implement Runnable
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */

public class ControlPanel extends JPanel implements Runnable {
	
	private static final long serialVersionUID = 2979654563137453846L;
	private static final Logger log = Logger.getLogger(ControlPanel.class.getPackage().getName());

	private static ControlPanel client;
	private static NunchuckPanel nunchuckPanel;
	private static StatsPanel statsPanel;
	
	private static RemoteCamera rc;
	private VideoPanel videoPanel;
	private ConfigPanel configPanel;
	private HelpPanel helpPanel;

	private LEDController sliderPanel;
	private static TextOutputPanel textOutputPanel;
	
	//GUI components
    public static JFrame frame;
	
	public ControlPanel() {
		
		super(new GridLayout(1, 1));
		
        JTabbedPane tabbedPane = new JTabbedPane();
        GridBagConstraints gbc;
        int[] i = { 0, 0, 0, 5};
            
        
        //	Build main panel
        JPanel mainPanel = new JPanel();
        mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		GridBagLayout gbl_mainPanel = new GridBagLayout();
		gbl_mainPanel.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_mainPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_mainPanel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_mainPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		mainPanel.setLayout(gbl_mainPanel);	
		
		//
        //	We build out the primary panels
		//
	
		statsPanel = StatsPanel.getInstance();	
		gbc = GridBagFactory.getGridBag(0, 0, 1, i, 3);
		mainPanel.add(statsPanel, gbc);
		
		//Build Video Box
        videoPanel = VideoPanel.getInstance(frame);	
        try {
			rc = RemoteCamera.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}

        //This adds the video panel to the frame
		gbc = GridBagFactory.getGridBag(0, 1, 0, i, 0);
		mainPanel.add(videoPanel, gbc);       
		
		textOutputPanel = TextOutputPanel.getInstance();
		
		gbc = GridBagFactory.getGridBag(10, 1, 2, i, 2);
		mainPanel.add(textOutputPanel, gbc);
		
		nunchuckPanel = NunchuckPanel.getInstance();
		gbc = GridBagFactory.getGridBag(0, 1, 1, i, 3);
		mainPanel.add(nunchuckPanel, gbc);	
		
		sliderPanel = new LEDController();
		gbc = GridBagFactory.getGridBag(0, 2, 1, i, 0);
		mainPanel.add(sliderPanel, gbc);
		
        configPanel = new ConfigPanel();        
		helpPanel = new HelpPanel();
		
		//Add all tabs to the control panel
        //Add panels to tabs on primary frame
        tabbedPane.addTab("Console", null, mainPanel,"Main Page");
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
        
        tabbedPane.addTab("Configuration", null, configPanel,"Configuration Page");
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
        
        tabbedPane.addTab("Help Center", null, helpPanel, "Help Page");
        tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
        
        //Add the tab pane to this panel.
        add(tabbedPane);
        
        //The following line enables to use scrolling tabs.
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

	}
	
	private static void buildGUI() {
				
		client = new ControlPanel();
		
		frame = new JFrame(Configuration.APP_NAME);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(client, BorderLayout.CENTER);    
        
        //Declare shutdown routine
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
        	    public void windowClosing(WindowEvent winEvt) {
        	        // TODO Nice - Perhaps ask user if they want to save any unsaved files first?
        	    	try {
        	    		RemoteCamera.getInstance().close();
        	    	} catch (Exception e) {
        	    		StackTraceUtil.getStackTrace(e);
        	    	}
        			log.log(Level.INFO, "Stopping camera capture...");
        	        synchronized(this) {
        	        	try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
        	        }
        	    }
        	});
        
        //frame.pack();
        
        //Center the screen
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setSize(screen.width, screen.height - 30);
		log.log(Level.CONFIG, "Screen Width: " + screen.width + "   Screen Height: " + screen.height);
        frame.setVisible(true);
        		
	}
	
	/**
	 * Program Entry Point
	 * Master thread controller and parent process
	 * 
	 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
	 */
	public static void main(String[] args) {		
		
		Handler fh;
		try {
			fh = new FileHandler("%t/MarineElectric.log");
			log.addHandler(fh);
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		SerialProperties.getProperties();
		ControlPanel.buildGUI();
		
		//Run test routine
		try {
			//TODO finish diagnostics
			//if(DiagnosticUnit.isOk() || true) {
			
			TextOutputPanel.getInstance().updateGUI("Launching ROV Client...");
			client.run();

		} catch (Exception e) {
			log.log(Level.SEVERE, "Yup, something is REALLY on fire...", e);
			log.log(Level.SEVERE, StackTraceUtil.getStackTrace(e));
		}
		
	}

	
	@Override
	public void run() {

		// Launch stats thread
		new Thread(new Runnable() {
			public void run(){
				try{
					ROVStat.getInstance().run();
				} catch (Exception e) {
					StackTraceUtil.getStackTrace(e);
				}
			}
		}).start();
		
		
		// Launch Webcam thread
		if(!Configuration.DISABLE_VIDEO) {
			new Thread(new Runnable() {
				public void run(){
					try{
						rc.run();
					} catch (Exception e) {
						StackTraceUtil.getStackTrace(e);
						rc.run();
					}
				}
			}).start();
		} else {
			log.log(Level.INFO, "Video Disabled...");
		}
		
		// Launch joystick thread
		new Thread(new Runnable() {
			public void run(){
				try{
					ROVJoyStick.getInstance().run();
				} catch (Exception e) {
					StackTraceUtil.getStackTrace(e);
				}
			}
		}).start();
	
	}
}
