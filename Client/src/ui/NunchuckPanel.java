/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import util.Configuration;

import nunchuck.ROVJoyStick;

/**
 * Nunchuck panel container - Holds controller user feedback
 * @param
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public class NunchuckPanel extends JPanel {
	
	protected static NunchuckPanel R;
	
	private static final long serialVersionUID = 7719320749986992741L;

	Point pt;
	JLabel monitor_lbl, anchor_lbl;
	JTextArea wiiMon;
	public static ROVJoyStick js;
	int[] i = { 0, 0, 2, 5};
	
	public static synchronized NunchuckPanel getInstance() {
		
		if(R == null) 
			R = new NunchuckPanel();
		return R;
		
	}
	
	private NunchuckPanel() {
		this.setSize(500, 500);
		GridBagConstraints gbc;
		GridBagLayout gbl_configPanel = new GridBagLayout();
		gbl_configPanel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_configPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_configPanel.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_configPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		this.setLayout(gbl_configPanel);		
		
		gbc = GridBagFactory.getGridBag(1, 1, 1, i, 1);
		this.add(anchor_lbl = new JLabel(" "), gbc);
		gbc = GridBagFactory.getGridBag(1, 2, 1, i, 1);
		this.add(monitor_lbl = new JLabel(Configuration.JOYSTICK_MONITOR_LBL), gbc);
		gbc = GridBagFactory.getGridBag(1, 3, 1, i, 1);
		
		//The scroll panel
		final JScrollPane wiiMonScroll = new JScrollPane(wiiMon = new JTextArea(24, 20),
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.add(wiiMonScroll, gbc);
		wiiMon.setEditable(false);
		wiiMon.setAutoscrolls(true);
		
	}

	public void updateGUI(final String s) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				wiiMon.append(s + "\n");
			}
		});

	}
}
