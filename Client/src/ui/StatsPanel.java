/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package ui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import util.Configuration;

/**
 * Stats panel container - ROV stats heads up panel
 * @param
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public class StatsPanel extends JPanel {

	protected static StatsPanel R;
	
	private static final long serialVersionUID = 711529114330969847L;

    private static JLabel charge;
	private JLabel temp;
	private JLabel etemp;
	private JLabel turbidity;
	private JLabel leak;
    int[] i = { 0, 0, 2, 5};
    
	public static synchronized StatsPanel getInstance() {
		
		if(R == null) 
			R = new StatsPanel();
		return R;
		
	}
	
	private StatsPanel() {
		GridBagConstraints gbc;
		setBackground(Color.WHITE);
		GridBagLayout gbl_statsPanel = new GridBagLayout();
		gbl_statsPanel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_statsPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_statsPanel.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_statsPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		
		JLabel chargeLbl  = new JLabel(Configuration.CHARGE_LBL, JLabel.RIGHT);
		JLabel tempLbl    = new JLabel(Configuration.TEMP_LBL, JLabel.RIGHT);
		JLabel etempLbl    = new JLabel(Configuration.E_TEMP_LBL, JLabel.RIGHT);
		JLabel lightLbl   = new JLabel(Configuration.TURBIDITY_LBL, JLabel.RIGHT);
		JLabel leakLbl    = new JLabel(Configuration.LEAK_LBL, JLabel.RIGHT);

		this.setLayout(gbl_statsPanel);
		
		gbc = GridBagFactory.getGridBag(1, 1, 1, i, 1);
		this.add(chargeLbl , gbc);
		
		gbc = GridBagFactory.getGridBag(15, 1, 2, i, 3);	
		this.add(charge = new JLabel(Configuration.INITIAL_VALUE, JLabel.LEFT), gbc);
		
		gbc = GridBagFactory.getGridBag(1, 2, 1, i, 1);	
		this.add(tempLbl, gbc);
		
		gbc = GridBagFactory.getGridBag(15, 2, 2, i, 3);
		this.add(temp = new JLabel(Configuration.INITIAL_VALUE, JLabel.LEFT), gbc);
		
		gbc = GridBagFactory.getGridBag(1, 3, 1, i, 1);	
		this.add(etempLbl, gbc);
		
		gbc = GridBagFactory.getGridBag(15, 3, 2, i, 3);
		this.add(etemp = new JLabel(Configuration.INITIAL_VALUE, JLabel.LEFT), gbc);
		
				
		gbc = GridBagFactory.getGridBag(1, 4, 1, i, 1);
		this.add(lightLbl, gbc);
		
		gbc = GridBagFactory.getGridBag(15, 4, 2, i, 3);
		this.add(turbidity = new JLabel(Configuration.INITIAL_VALUE, JLabel.LEFT), gbc);
		
		gbc = GridBagFactory.getGridBag(1, 5, 1, i, 1);
		this.add(leakLbl, gbc);
		
		gbc = GridBagFactory.getGridBag(15, 5, 2, i, 3);
		this.add(leak = new JLabel(Configuration.INITIAL_VALUE, JLabel.LEFT), gbc);
	}
	
	//Setters for the control panel (all safely of course)
	public void setCharge(final String c) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				charge.setText(c + " %");
			}
		});
	}
	
	public void setTemp(final String c) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				temp.setText(c + " �C");
			}
		});
	}
	
	public void seteTemp(final String c) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				etemp.setText(c + " �C");
			}
		});
	}
	
	
	public void setTurbidity(final String c) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				turbidity.setText(c + " Lumens");
			}
		});
	}
	
	public void setLeak(final String c) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				leak.setText(c);
			}
		});
	}
	
	public void setEmergency() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				setBackground(Color.RED);
			}
		});
	}

	public void unSetEmergency() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				setBackground(Color.WHITE);
			}
		});		
	}
}
