/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package ui;

import javax.swing.JEditorPane;
import javax.swing.JPanel;

public class HelpPanel extends JPanel{

	private static final long serialVersionUID = -7045224682506804667L;

	public HelpPanel() {
		JEditorPane directions;
		this.add(directions = new JEditorPane());
		directions.setText("Visit http://marineelectric.corytodd.us");
		directions.setEditable(false);
		directions.setSize(400, 800);	
	}
}
