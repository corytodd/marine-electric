/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package serial;

import java.util.logging.Level;
import java.util.logging.Logger;

import ui.ControlPanel;
import ui.StatsPanel;
import ui.TextOutputPanel;
import util.Configuration;
import util.StackTraceUtil;

/**
 * ROVStat class - CentralNexus impl
 * @extends Thread
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public class ROVStat implements Runnable {

	protected static ROVStat R;
	private static final Logger log = Logger.getLogger(ROVStat.class.getPackage().getName());

	private static String turbidity;
	
	public static synchronized ROVStat getInstance() {
		if (R == null) 
			R = new ROVStat();
		return R;
	}
	
	private ROVStat() { 
		updateTop("StatsListener Initialized...");
	}
	
	public static synchronized void retrieveData() {
		
		try {
			String resp;
			ModConnector mt = ModConnector.getInstance();
			updateTop("Checking ROV Stats...");
			mt.setAction( Configuration.READ_REF, 6, 1 );
			ModConnector.getInstance().run();	
			resp = mt.getReadResponse();
			updateTop("Raw stats: \n" + resp);
	
			// Data is returned as a space delimited String
			String[] respArray = resp.split(" ");
			
			/*
			 * 4 [4]  - Used to Express the Turbidity of the Water (LED Activated)  (Upper 15-8 Bits)
			 * 5 [4]  - Used to Express the Turbidity of the Water (LED Activated) (Lower 7-0 Bits) 
			 * 6 [5] - Used to Express the Turbidity of the Water (LED De-Activated) (Upper 15-8 Bits) (ADC - 84)
			 * 7 [5] - Used to Express the Turbidity of the Water (LED De-Activated)(Lower 7-0 Bits) (ADC - 84)
			 * 8 [6] - Used to Store the Internal (Chamber) Temperature Sensor Reading (Upper 15-8 Bits) (ADC - 86)
			 * 9 [6] - Used to Store the Internal (Chamber) Temperature Sensor Reading (Lower 7-0 Bits) (ADC - 86)
			 * 10 [7] - Used to Store the External (Water) Temperature Sensor Reading (Upper 15-8 Bits) (ADC - 80)
			 * 11 [7] - Used to Store the External (Water) Temperature Sensor Reading (Lower 7-0 Bits)  (ADC - 80)
			 * 12 [8] - Battery Level Voltage Reading (Upper 15-8 Bits) (ADC - 82)
			 * 13 [8] - Battery Level Voltage Reading (Lower 7-0 Bits) (ADC - 82)
			 * 14 [9] - Water Detection (0x0F) - Sensor 1  ,  (0xF0) - Sensor 2
			 *     Added by David to make MODBUS register number correct - MODBUS uses 16 bits, and we have 10 values
			 *     to monitor/change which equates to 20 bytes....
			 * 15 [9]
			 */
			
			StatsPanel.getInstance().setCharge(Integer.parseInt(respArray[12], 16) + "");
			StatsPanel.getInstance().setTemp(Integer.parseInt(respArray[8], 16) + "");
			StatsPanel.getInstance().seteTemp(Integer.parseInt(respArray[10], 16) + "");
			
			// Use data from whichever register has a value
			turbidity = (Integer.parseInt(respArray[4]) == 0) ? respArray[6] : respArray[4];
			StatsPanel.getInstance().setTurbidity(Integer.parseInt(turbidity, 16) + "");
			
			
			//TODO create "Panic look" when leaking
			if(Integer.parseInt(respArray[13], 16) + Integer.parseInt(respArray[14], 16) != 0) {
				StatsPanel.getInstance().setLeak("True!!");
				StatsPanel.getInstance().setEmergency();
			}
			else {
				StatsPanel.getInstance().setLeak("No");
				StatsPanel.getInstance().unSetEmergency();
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, StackTraceUtil.getStackTrace(e), e);
		}
		
	}

	@Override
	public void run() {
		
		while (true) {
			synchronized (this) {
				retrieveData();
				try {
					// We wait 10 seconds between reads to achieve 
					//   a more responsive write controller
					this.wait(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	private static void updateTop(String string) {
		log.log(Level.INFO, string);
		TextOutputPanel.getInstance().updateGUI(string);
	}
		
}
