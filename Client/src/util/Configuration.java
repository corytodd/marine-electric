/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package util;

public class Configuration {
	
	//System level
	public static final String APP_VERSION 		= "Release 1.1";
	public static final String APP_NAME 		= "Marine Electric ROV Command Center";
	public static final String LOADING_TEXT		= "Loading...";
	public static final String DEFAULT_CAMERA 	= "USB 2.0 PC Cam";
	
	//Stats dialog
	public static final String CHARGE_LBL 		= "Battery Charge:";
	public static final String TEMP_LBL 		= "Onboard Temperature:";
	public static final String E_TEMP_LBL		= "External Temperature:";
	public static final String TURBIDITY_LBL 	= "Water Turbidity:";
	public static final String LEAK_LBL 		= "Water Leak Detected:";
	public static final String INITIAL_VALUE 	= "Waiting for data...";
	
	//Joystick dialog
	public static final String JOYSTICK_MONITOR_LBL = "Joystick Coordinates";
	
	//TextOutput Panel
	public static final String TEXT_OUTPUT_LABEL = "System Monitor";	
	
	/* DEFAULT VALUES ONLY USED IN THE ABSENSE OF THE CONFIG FILE */
	//Serial connection
	public static String PORT_NAME= "COM9"; 			// the name of the serial port to be used
	public static int BAUD_RATE= 9600;					// Modbus baud rate
	public static String ENCODING = "ascii";			
	
	// Video Settings
	public static final boolean DISABLE_VIDEO = false;
	public static int VIDEO_WIDTH = 320;
	public static int VIDEO_HEIGHT = 240;
	public static int LED_POWER = 75;
	
	// Non-user customizable fields
	public static boolean IS_INVERTED;					// Invert the Z and C buttons
	public static final int DATA_BITS = 8;				// 
	public static final String PARITY = "NONE";			//
	public static final int STOP_BITS = 1;				//
	public static final boolean ECHO = true;			//
	public static final int SLAVE_ID = 1; 				// the unit identifier we will be talking to
	public static final int MASTER_ID = 1;				// this app
	public static boolean CALCULATE_CHECKSUM = false;	// Buggy LRC
	public static final int TIMEOUT_MS_SCI = 3000;		// Timeout for all MODBUJS Comms
	
	// All references are indices (i.e. start at 0)
	public static final int READ_REF = 4; 				// the reference, where to start reading from
	public static final int WRITE_REF = 0;				// the reference of where to start writing
	public static final int COUNT = 0; 					// the count of IR's to read
	public static final int REPEAT = 1;					// how many times to attempt transaction

	public Configuration() { }
	
	public static void setPort(String port) {
		PORT_NAME = port;
	}
	
	public static void setBaud(int baud) {
		BAUD_RATE = baud;
	}
	
	public static void setEncoding(String encoding) {
		ENCODING = encoding;
	}

	public static void setLEDPower(int power) {
		LED_POWER = power;
	}
}
