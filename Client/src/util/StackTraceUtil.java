/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package util;

import java.io.*;

/**
 * Print stack trace to UI for debugging
 * 
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */

public final class StackTraceUtil {

	/**
	 * Print stack trace to UI for debugging
	 * 
	 * @param Throwable e
	 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
	 */
	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	/**
	 * Print stack trace to UI for debugging
	 * 
	 * @param Throwable e
	 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
	 */
	public static String getCustomStackTrace(Throwable aThrowable) {
		// add the class name and any message passed to constructor
		final StringBuilder result = new StringBuilder("[MarineElectric]");
		result.append(aThrowable.toString());
		final String NEW_LINE = System.getProperty("line.separator");
		result.append(NEW_LINE);

		// add each element of the stack trace
		for (StackTraceElement element : aThrowable.getStackTrace()) {
			result.append(element);
			result.append(NEW_LINE);
		}
		return result.toString();
	}

}
