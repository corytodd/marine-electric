/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package util;

/**
 * Enumerate List entries for serial configuration
 * @param Throwable e
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public enum BaudRate {
	
	B300 ("300"), B600 ("600"), B1200 ("1200"), B2400 ("2400"), B4800 ("4800"),
	B9600 ("9600"), B14400 ("14400"), B19200 ("19200"), B38400 ("38400"), 
	B56000 ("56000"), B57600 ("57600"), B115200 ("115200"), B128200 ("128200"),
	B256000 ("256000");
	
	private final String baud;
	BaudRate(String str) {
		this.baud = str;
	}
	
	public String portName() {
		return this.baud;
	}
	
}

