/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package util;

/**
 * Enumerate List entries for serial configuration
 * @param Throwable e
 * @author <a href="mailto:cory@corytodd.us">Cory Todd</a>
 */
public enum ComPort {
	
	COM1 ("COM1"), COM2 ("COM2"), COM3 ("COM3"), COM4 ("COM4"), COM5 ("COM5"), 
	COM6 ("COM6"), COM7 ("COM1"), COM8 ("COM1"), COM9 ("COM1"), COM10 ("COM1"), 
	COM11 ("COM1"), COM12 ("COM1"), COM13 ("COM1"), COM14 ("COM1"), COM15 ("COM1"), 
	COM16 ("COM1"), COM17 ("COM1"), COM18 ("COM1"), COM19 ("COM1"), COM20 ("COM1"), 
	COM21 ("COM1"), COM22 ("COM1"), COM23 ("COM1"), COM24 ("COM1"), COM25 ("COM1");
	
	private final String port;
	ComPort(String str) {
		this.port = str;
	}
	
	String portName() {
		return this.port;
	}
	
}

