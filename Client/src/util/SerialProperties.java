/*******************************************************************************
 * Copyright (c) 2012 Marine Electric.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Marine Electric - initial API and implementation
 ******************************************************************************/
package util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class SerialProperties {

	private static SerialProperties R;

	public static synchronized SerialProperties getInstance() {
		if (R == null)
			R = new SerialProperties();
		return R;
	}

	private SerialProperties() {	}
	
	public static void setProperties(String baud, String port, String encoding) {

		Properties properties = new Properties();

		// Write properties file.
		try {
			properties.setProperty("commPort", port);
			properties.setProperty("baudRate", baud);
			properties.setProperty("encoding", encoding);
		    properties.store(new FileOutputStream("ROV.config"), null);
		} catch (IOException e) {
		}
	}

	public static void getProperties() {
		try {
			// Read properties file.
			Properties properties = new Properties();
			properties.load(new FileInputStream("ROV.config"));
			
			Configuration.setBaud(isIntegerParseInt(properties.getProperty("baudRate")) 
					?  Integer.parseInt(properties.getProperty("baudRate")) : (Configuration.BAUD_RATE));
			Configuration.setPort(properties.getProperty("commPort", Configuration.PORT_NAME));
			Configuration.setEncoding(properties.getProperty("encoding", Configuration.ENCODING));

		}
		catch (NumberFormatException nfe) {
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static boolean isIntegerParseInt(String str) {
		try {
			Integer.parseInt(str);
			return true;
			} catch (NumberFormatException nfe) {}
		return false;
		}
	
	/*
	public static void main(String[] args) {
		String[] a = new String[3];
		a[0] = "COM4";
		a[1] = "9600";
		a[2] = "ascii";
		//setProperties(a);
		getProperties();
	}
	*/
}
