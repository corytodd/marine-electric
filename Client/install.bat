#Install Script for Marine Electric ROV

BIT=
 
@echo off
 
Set RegQry=HKLM\Hardware\Description\System\CentralProcessor\0
 
REG.exe Query %RegQry% > checkOS.txt
 
Find /i "x86" < CheckOS.txt > StringCheck.txt
 
If %ERRORLEVEL% == 0 (
    Echo "Installing for 32-bit OS"
	BIT=32
) ELSE (
    Echo "Installing for 64-bit OS"
	BIT=64
)
 
copy %BIT%\rxtxSerial.dll C:\Windows
copy win32com.dll C:\Windows

ECHO "Done!"

java -jar MarineElectric.jar

pause