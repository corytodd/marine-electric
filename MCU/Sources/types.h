/*******************************************************************************
*           Copyright (c) 2012  Marine Electronics		                       *
********************************************************************************

           @file 	types.h
		   @author	David Vernon
           @version 1.00
           @date	8/5/2012
           @brief	File provided type definitions and macros for ROV Project

*/		   
/******************************************************************************/
		   
/********************           COMPILE FLAGS                ******************/
#ifndef _TYPES_H
    #define _TYPES_H

/********************           INCLUDE FILES                ******************/

/********************               ENUMS                    ******************/

/********************              DEFINES                   ******************/
#define PRIVATE  static  /* Only functions within module can access this function/variable */ 
#define GLOBAL   extern	 /* Anyone can use this variable/call this function */
/********************               MACROS                   ******************/
/* Generic macros */
#ifdef DEBUG
   #define TEST(x)     x
#else
   #define TEST(x)     
#endif

#define MIN(x,y)           ( ((x)<(y)) ? (x) : (y) )
#define MAX(x,y)           ( ((x)>(y)) ? (x) : (y) )

#define  MSB(word)         ((word) >> 8)
#define  LSB(word)         ((word) & 0xff)

#define ROUND(a)           ((int)(a+0.5))

#define COMPARE(a,b,c)     {if (a<b) b = a; if (a>c) c = a;}
#define SIGN(x)            ((x) > 0 ? 1:  ((x) == 0 ? 0:  (-1)))
#define SWAP(a,b)          {(a)^=(b); (b)^=(a); (a)^=(b);}
#define ABS( x )           ( ( (x) >= 0 ) ? (x) : -(x) )

#define ATOMIC(x)          {DISABLE; x; ENABLE;}

#define DO_FOREVER         for(;;) */
/********************         TYPE DEFINITIONS               ******************/
typedef unsigned char      BYTE;
typedef signed char        SBYTE;
typedef unsigned char      U8;
typedef signed char        S8;

typedef unsigned short     U16;
typedef signed short       S16;
typedef unsigned short     WORD;
typedef signed short       SWORD;

typedef unsigned long       U32;
typedef signed long         S32;
typedef unsigned long       DWORD;
typedef signed long         SDWORD;

typedef unsigned char      BOOLEAN;

/********************        FUNCTION PROTOTYPES             ******************/

/********************          LOCAL VARIABLES               ******************/

/********************          GLOBAL VARIABLES              ******************/

/********************              FUNCTIONS                 ******************/

#endif
