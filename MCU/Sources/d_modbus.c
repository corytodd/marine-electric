/*******************************************************************************
*           Copyright (c) 2012  Marine Electronics		                       *
********************************************************************************

           @file 	d_modbus.c
		   @author	David Vernon
           @version 1.00
           @date	8/5/2012
           @brief	Funcitons and Methods for the MODBUS Driver - ASCII version

*/
/******************************************************************************/

/********************           COMPILE FLAGS                ******************/

/********************           INCLUDE FILES                ******************/
#include <hidef.h> 
#include "derivative.h"
#include "types.h"
#include "d_modbus.h"
#include "UROV_Functions.h"

/********************               ENUMS                    ******************/
typedef enum
{
   WAIT_FOR_CHAR = 1,
   PROCESS_RX,
   PROCESS_TX,
   TX_COMPLETE
}MODBUS_COMM_STATES;
/********************              DEFINES                   ******************/

/********************               MACROS                   ******************/

/********************         TYPE DEFINITIONS               ******************/

/********************        FUNCTION PROTOTYPES             ******************/
void    MODBUS_vInitialize(void);
void    MODBUS_vProcessRxData(void);
void    MODBUS_vProcessTxData(void);
void    MODBUS_vReadData(void);
void    MODBUS_vWriteData(void);
Bool    MODBUS_bProcessAddr(void);
void    MODBUS_vProcessError(void);
Bool    MODBUS_bCheckLRC(void);
U16     MODBUS_u16AsciiToHex(U8 *pu8Buffer);
U8      MODBUS_u8AsciiToHex(U8 *pu8Buffer);
void    MODBUS_u16HexToAscii(U16 u16RegValue, U8 *pu8Buffer);
void    MODBUS_u8HexToAscii(U8 val, U8 *pu8Buffer);

U8      Bin2Ascii(U8 c);
U8      Ascii2Bin(U8 c);


/********************          LOCAL VARIABLES               ******************/

typedef struct {
   byte bRead       : 1;
   byte bCommError  : 1;
   byte bProcessTX  : 1;
   byte bStart      : 1;
   byte bIllegalFunc  : 1;
   byte bIllegalAddr  : 1;
   byte bIllegalValue : 1;
}MFLAG; //ModbusFlags;

MFLAG ModbusFlags;

U8  u8ModbusCommand, u8ModbusAddress, u8LCR_Count, u8ModbusState; 
U8  u8ModbusByteCount, u8ModbusError, u8ModbusTxByteCount;
U8  *pu8ModbusBuffer;
U8  u8ModbusBuffer[MODBUS_BUFFER];

U16 u16ModbusRegStart, u16ModbusRegCount;

/********************          GLOBAL VARIABLES              ******************/

/********************              FUNCTIONS                 ******************/
/* FUNCTION: MODBUS_vCommState ************************************************/
/**
* @brief  MODBUS State Hopper - Called once through main loop
* @param  None
* @retval Nothing
*/
void MODBUS_vCommState( void )
{
    /* Start of MODBUS State Machine */     
    switch (u8ModbusState)
    {
	    /* Case to wait for characters to be received, will hop to this state until
		 a complete sentence is received */
        case WAIT_FOR_CHAR:
		    /* Quick return to main task until message is received */
		break;
      
	    /* Case to process the received data */
        case PROCESS_RX:
		    /* Processes the received data and determines if a reply is necessary
		    if a reply is necessary it sets the next state to be PROCESS_TX which
			it will hop to the next time this method is called from main */
		    MODBUS_vProcessRxData();
		    u8ModbusState = PROCESS_TX;
		    Delay_ms(MODBUSTXDLY);
	    break;
        
        /* Case to Transmit the reponse data and reset state machine for next
         message */      
        case PROCESS_TX:
		    MODBUS_vProcessTxData();
		    MODBUS_vInitialize();
		break;
      
       default:
	     break;
   }
}

/* FUNCTION: MODBUS_vInitialize ***********************************************/
/**
* @brief  Initialized Modbus Task Variables and Initial task
* @param  None
* @retval Nothing
*/
void MODBUS_vInitialize (void)
{
    //Initialize Pointers and Counters
    pu8ModbusBuffer = &u8ModbusBuffer[0];
    u8ModbusByteCount = 0;
    ModbusFlags.bStart = FALSE;
    ModbusFlags.bCommError = FALSE;
   
    //SET FIRST STATE TO USE
    u8ModbusState = WAIT_FOR_CHAR;
}

/* FUNCTION: MODBUS_vProcessRxData ********************************************/
/**
* @brief  Processes the Received MODBUS frame
* @param  None
* @retval Nothing
*/
void MODBUS_vProcessRxData (void)
{
    if( MODBUS_bCheckLRC() )
    {
        // Get the address from the message
	    u8ModbusAddress = MODBUS_u8AsciiToHex(&u8ModbusBuffer[MODBUS_ADDRESS]);
	            	  
	    // Verify that the message is for us 	
	    if ( u8ModbusAddress == UNIT_ADDR )
	    {		 
	        //Next, verify the command and process it
		    u8ModbusCommand = MODBUS_u8AsciiToHex(&u8ModbusBuffer[MODBUS_COMMAND]);

  		    //Is it READ or WRITE.  Otherwise it's invalid and will be handled by
		    //the error processing function
		    switch (u8ModbusCommand)
   		    {
		        case MODBUS_READ: 
			        MODBUS_vReadData();
			    break;
			    case MODBUS_WRITE: 
			        MODBUS_vWriteData();
			    break;
			    default: 
			        ModbusFlags.bIllegalFunc = TRUE;
			        MODBUS_vProcessError();
		    }
		    //All done with processing and ready to respond
		    //ModbusFlags.bProcessTX = TRUE;
        }	
    }
    return;
}

/* FUNCTION: MODBUS_vProcessTxData ********************************************/
/**
* @brief  Processes the response, builds the table and transmits reply
* @param  None
* @retval Nothing
*/
void MODBUS_vProcessTxData (void)
{
   U16 u16Idx;

   /* Set the Start Byte */
   u8ModbusBuffer[0] = START;
   
   /* Clear thte LCR count and initialize pointer to buffer */	
   u8LCR_Count = 0;
   pu8ModbusBuffer = &u8ModbusBuffer[1];
	
   /* Set the SLAVE ADDRESS and COMMAND in the buffer */		
   MODBUS_u8HexToAscii(u8ModbusAddress, pu8ModbusBuffer);
   pu8ModbusBuffer += 2;
   MODBUS_u8HexToAscii(u8ModbusCommand, pu8ModbusBuffer);
   pu8ModbusBuffer += 2;
      
   /* Determine which command was sent and respond appropriately */
   if(ModbusFlags.bRead && !ModbusFlags.bCommError)  /* Command was a READ */
   {
      /* Set the NUMBER OF BYTES */
	  MODBUS_u8HexToAscii(u8ModbusByteCount, pu8ModbusBuffer);
      pu8ModbusBuffer += 2;
	  /* Fill buffer with values from register array */
	  for(u16Idx = u16ModbusRegStart; u16Idx < (u16ModbusRegStart + u16ModbusRegCount); u16Idx++)
	  {
	     MODBUS_u16HexToAscii( REG_u16Read( u16Idx ), pu8ModbusBuffer );
	     pu8ModbusBuffer += 4;
	  }    			
   }
   else if(!ModbusFlags.bRead && !ModbusFlags.bCommError) /* Command was a WRITE */
   {
      /* Set the START ADDRESS and the REGISTER COUNT values */
	  MODBUS_u16HexToAscii(u16ModbusRegStart, pu8ModbusBuffer);
      pu8ModbusBuffer += 4;
	  MODBUS_u16HexToAscii(u16ModbusRegCount, pu8ModbusBuffer);
	  pu8ModbusBuffer += 4;
   }
   else /* Unknown Command - ERROR */
   {
       MODBUS_u8HexToAscii(u8ModbusError, pu8ModbusBuffer);
       pu8ModbusBuffer +=2;
	   ModbusFlags.bCommError = FALSE; /* Clear the Error Flag */
   }

   /* Set LCR value and end of message characters */
   MODBUS_u8HexToAscii( ((U8)(-(char)u8LCR_Count)), pu8ModbusBuffer);
   pu8ModbusBuffer += 2;
   *pu8ModbusBuffer = CARRAGE_RETURN;
   pu8ModbusBuffer++;
   *pu8ModbusBuffer = LINE_FEED;

   /* Get total number of bytes to be transmitted for DMA */
   u8ModbusTxByteCount = (U8)(pu8ModbusBuffer - &u8ModbusBuffer[0]);
  
   SCI_Transmitter( &u8ModbusBuffer[0], (u8ModbusTxByteCount + 1) ); 			
   return;
}								 

/* FUNCTION: MODBUS_vReadData *************************************************/
/**
* @brief  Verifies that the READ command has valid paramters
* @param  None
* @retval Nothing
*/
void MODBUS_vReadData (void)
{
	/* This is a Read command */
	ModbusFlags.bRead = TRUE;
   
	/* Check the MODBUS Addresses first */
	if( !MODBUS_bProcessAddr() )
	{
		/* Address was not valid, update the Error Flags */	
		 MODBUS_vProcessError();
	}
	return;
	
}

/* FUNCTION: MODBUS_vWriteData ************************************************/
/**
* @brief  Verifies that the Write Command has valid data
* @param  None
* @retval Nothing
*/
void MODBUS_vWriteData (void)
{
	U16 u16Idx;

	/* This is a Write command */
	ModbusFlags.bRead = FALSE;

	/* Check the MODBUS Addresses first */
	if( !MODBUS_bProcessAddr() ) 
	{
		/* Address was not valid, update the Error Flags */	
		MODBUS_vProcessError();
	}
	else
	{
		/*Initialize the pointer to the actual data in the RX string */
		pu8ModbusBuffer = &u8ModbusBuffer[MODBUS_DATA];
	  
		/* Write the data from the buffer into the register array */	
		for (u16Idx = u16ModbusRegStart; u16Idx < (u16ModbusRegStart + u16ModbusRegCount); u16Idx++)
		{
			REG_vWrite( u16Idx, MODBUS_u16AsciiToHex(pu8ModbusBuffer) );
			pu8ModbusBuffer += 4;
		}
	}
	return;
}

/* FUNCTION: MODBUS_bProcessAddr **********************************************/
/**
* @brief  Verifies that the requested Registers are valid for READ and WRITE
* @param  None
* @retval Bool: TRUE = Request is Valid, FALSE = Invalid Request
*/
Bool MODBUS_bProcessAddr (void)
{
  /* Get the START Address and the COUNT */
  u16ModbusRegStart = MODBUS_u16AsciiToHex(&u8ModbusBuffer[MODBUS_START]);
   u16ModbusRegCount = MODBUS_u16AsciiToHex(&u8ModbusBuffer[MODBUS_COUNT]);

   /*Check and see if registers requested are out of bounds */
   if ( u16ModbusRegStart > REGISTER_SIZE )
   {
      /* Get here -> Start Address is too high */
	  ModbusFlags.bIllegalAddr = TRUE;
	  return FALSE;
   }
   else if (u16ModbusRegCount > REGISTER_SIZE)
   {
      /* Get here -> Requested too many registers */
	  ModbusFlags.bIllegalValue = TRUE;
	  return FALSE;
   }
   else
   {
   /* Actual byte count is HEX bytes */
   u8ModbusByteCount = u16ModbusRegCount * 2;
   return TRUE;	
   }
}

/* FUNCTION: MODBUS_vProcessError *********************************************/
/**
* @brief  Sets various flags depending on the Error Flag that is set
* @param  None
* @retval Nothing
*/
void MODBUS_vProcessError( void )
{
	/* If an error, set the MSB of the current command */
	u8ModbusCommand = u8ModbusCommand + 128;
	
	/* The u8ModbusError number relates to the error number in the MODBUS spec */
	/* Once the u8ModbusError is set the related flags are cleared */
	if ( ModbusFlags.bIllegalFunc )
	{
		u8ModbusError = 1;
		ModbusFlags.bIllegalFunc = FALSE;
	}
	else if ( ModbusFlags.bIllegalAddr )
	{
		u8ModbusError = 2;
		ModbusFlags.bIllegalAddr = FALSE;
	}
	else if ( ModbusFlags.bIllegalValue )
	{
		u8ModbusError = 3;
		ModbusFlags.bIllegalValue = FALSE;
	}
	
	/* Yes we have an error */
	ModbusFlags.bCommError = TRUE;
	return;
}

/* FUNCTION: MODBUS_bCheckLRC ************************************************/
/**
* @brief  Checks the LRC value of the incoming message frame
* @param  None
* @retval Bool: TRUE - LRC is OK, FALSE - LRC is invalid
*/
Bool MODBUS_bCheckLRC (void)
{
   U8 Temp1, u8Idx;
   
   u8ModbusByteCount = (U8)((u8ModbusByteCount - 4) / 2 );	
   pu8ModbusBuffer = &u8ModbusBuffer[0];
   u8LCR_Count = 0;

   for (u8Idx = 0 ; u8Idx < u8ModbusByteCount; u8Idx++)
   {
      	u8LCR_Count += MODBUS_u8AsciiToHex(pu8ModbusBuffer);
		pu8ModbusBuffer += 2;
   }

   u8LCR_Count = (U8)(-(char)u8LCR_Count);
   Temp1 = MODBUS_u8AsciiToHex(pu8ModbusBuffer);

   #if MODBUS_USE_LRC
		if ( Temp1 == u8LCR_Count)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	#else
		return TRUE;
	#endif
}

/* FUNCTION: MODBUS_vRxData ***************************************************/
/**
* @brief  Designed to be called in the SCI IRQ to process a character at a time
* @param  None
* @retval Nothing
*/
void MODBUS_RxData( U8 u8Char )
{
   /* Loads character from receiver buffer to ModbusRxBuffer */	
   *pu8ModbusBuffer = u8Char;
   u8ModbusByteCount++;
   
   if ( *pu8ModbusBuffer == START )
   {
	  /* If the character is START then we reset, and exit to wait for next char */
	  ModbusFlags.bStart = TRUE;
      u8ModbusByteCount = 0;
      pu8ModbusBuffer = &u8ModbusBuffer[0];
	  return;
   }

   if ( ModbusFlags.bStart )
   {
   	  /* Received a START char so now it loads 1 char per IRQ and tests for END char */
	  if ( *pu8ModbusBuffer == LINE_FEED )
      {
	     /* Received the LAST char so now it changes states and re-initializes variables */		
		 u8ModbusState = PROCESS_RX;
		 ModbusFlags.bStart = FALSE;
	     pu8ModbusBuffer = &u8ModbusBuffer[0];
         return;
      }
	  /* Point to next location for next time to IRQ */
      pu8ModbusBuffer++;

      /* Make sure that pointer is not past the end of the buffer */
      if ( pu8ModbusBuffer > &u8ModbusBuffer[(MODBUS_BUFFER-1)] )
      {
		 /* If at the end then it resets and goes back to looking for START char */	
		 u8ModbusByteCount = 0;
         pu8ModbusBuffer = &u8ModbusBuffer[0];
		 ModbusFlags.bStart = FALSE;
      }
   }
   return;
}

/*****  H E X - A S C I I  \  A S C I I - H E X   C O N V E R S I O N *********/
/* FUNCTION: MODBUS_vCommState ************************************************/
/**
* @brief  Converts 4 ASCII characters to a 16 bit hex value
* @param  *pu8Buffer: Pointer to Byte Array that contains value to convert
* @retval U16: 16-bit HEX value of the 4 ASCII characters
*/
U16 MODBUS_u16AsciiToHex (U8 *pu8Buffer)
{
    U16  Temp1;
  
    Temp1 = (ASCII2BIN(*pu8Buffer) << 12);
    pu8Buffer++;
    Temp1 += (ASCII2BIN(*pu8Buffer) << 8);
    pu8Buffer++;
    Temp1 += (ASCII2BIN(*pu8Buffer) << 4);
    pu8Buffer++;
    Temp1 += ASCII2BIN(*pu8Buffer);
  
    return Temp1;
}

/* FUNCTION: MODBUS_vu8AsciiToHex *********************************************/
/**
* @brief  Converts 2 ASCII characters to a 8-bit hex value
* @param  *pu8Buffer: Pointer to Byte Array that contains value to convert
* @retval U8: 8-bit hex value of the 2 ASCII characters
*/
U8 MODBUS_u8AsciiToHex (U8 *pu8Buffer)
{
    U8 Temp1; 
    Temp1 = (ASCII2BIN(*pu8Buffer) << 4);
    pu8Buffer++;
    Temp1 += ASCII2BIN(*pu8Buffer);
    return Temp1;

}

U8 Ascii2Bin(U8 c)
{
   return ( (c <=0x39) ? (c-0x30) : (c-0x37) );
} 

/* FUNCTION: MODBUS_u16HexToAscii *********************************************/
/**
* @brief  Converts a 16-bit value to four ASCII characters
* @param  u16RegValue: Value to be converted
* @param  *pu8Buffer: Pointer to Byte Array location to place results
* @retval Nothing
*/
void MODBUS_u16HexToAscii (U16 u16RegValue, U8 *pu8Buffer)
{
    u8LCR_Count += MSB(u16RegValue);
    u8LCR_Count += LSB(u16RegValue);

    *pu8Buffer++ = Bin2Ascii((u16RegValue & 0xF000) >> 12);
    *pu8Buffer++ = Bin2Ascii((u16RegValue & 0x0F00) >> 8);
    *pu8Buffer++ = Bin2Ascii((u16RegValue & 0x00F0) >> 4);
    *pu8Buffer   = Bin2Ascii(u16RegValue & 0x000F);
}

/* FUNCTION: MODBUS_u8HexToAscii **********************************************/
/**
* @brief  Converts a 8-bit value to two ASCII characters
* @param  u16RegValue: Value to be converted
* @param  *pu8Buffer: Pointer to Byte Array location to place results
* @retval Nothing
*/
void MODBUS_u8HexToAscii(U8 val, U8 *pu8Buffer)
{
    u8LCR_Count += val;
   
    *pu8Buffer++ = Bin2Ascii((val & 0xF0) >> 4);
    *pu8Buffer   = Bin2Ascii(val & 0x0F);
}   

U8 Bin2Ascii(U8 c) 
{
   return ( (c<=9) ? ('0'+ c) : ('A'+ c - 10) );
}


