/*******************************************************************************
*           Copyright (c) 2012  Marine Electronics		                       *
********************************************************************************

           @file 	d_modbus.h
		   @author	David Vernon
           @version 1.00
           @date	8/5/2012
           @brief	Header file for the Modbus driver d_modbus.c file

*/		   
/******************************************************************************/

/********************           COMPILE FLAGS                ******************/
#ifndef D_MODBUS_H
   #define D_MODBUS_H
   
#define MODBUS_USE_LRC		0	/* Change to 1 if you want to verify LRC */   
/********************           INCLUDE FILES                ******************/
#include "types.h"

/********************               ENUMS                    ******************/

/********************              DEFINES                   ******************/
#define UNIT_ADDR    1

#define MODBUS_ADDRESS   0
#define MODBUS_COMMAND   2
#define MODBUS_START     4 
#define MODBUS_COUNT     8
#define MODBUS_DATA	    14

#define MODBUS_READ    0x03
#define MODBUS_WRITE   0x10
#define GLOBAL_ADDR	   0

#define START		   0x3A
#define CARRAGE_RETURN 0x0D
#define LINE_FEED      0x0A

#define MODBUSTXDLY	   3
#define MODBUSRXDLY	   1

#define MODBUS_BUFFER    64	/* Max Registers set for 80 */
#define MODBUS_MAXCNT    ((MODBUS_BUFFER - 12) / 4)

/********************               MACROS                   ******************/
#define ASCII2BIN(val) 	((val <= 0x39)? val - 0x30 : val - 0x37)
#define BIN2ASCII(val) 	((val <= 9)? '0'+ val : 'A'+ val - 10)

/********************         TYPE DEFINITIONS               ******************/

/********************        PUBLIC FUNCTION PROTOTYPES      ******************/
void MODBUS_vInitialize(void);
void MODBUS_vCommState ( void );
void MODBUS_RxData( byte u8Char );
          
/********************          LOCAL VARIABLES               ******************/

/********************          GLOBAL VARIABLES              ******************/

/********************              FUNCTIONS                 ******************/

#endif