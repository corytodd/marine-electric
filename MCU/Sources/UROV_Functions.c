/*******************************************************************************
*           Copyright (c) 2012  Marine Electronics		                       *
********************************************************************************

           @file 	UROV_Funtions.c
		   @author	Darrell Pearson
           @version 1.00
           @date	8/5/2012
           @brief	File provided type definitions and macros for ROV Project
*/		   
/******************************************************************************/
/********************           COMPILE FLAGS                ******************/

/********************           INCLUDE FILES                ******************/
#include <hidef.h> 
#include "derivative.h"
#include "types.h"
#include "UROV_Functions.h"

/********************               ENUMS                    ******************/

/********************              DEFINES                   ******************/

/********************               MACROS                   ******************/

/********************         TYPE DEFINITIONS               ******************/

/********************        PUBLIC FUNCTION PROTOTYPES      ******************/

/********************          LOCAL VARIABLES               ******************/

/********************          GLOBAL VARIABLES              ******************/

unsigned char Data_Array[ARRAY_SIZE] = {0x00,0x02,0x00,0x7F,0x00,0x7F,
                                0x00,0x00,0x00,0x00,0x00,0x00,
                                0x00,0x00,0x00,0x00,0x00,0x00,
                                0x00,0x00};

unsigned char ST_1;             // Stores Sensor Sample Timer Data 
unsigned char Sample_Sensors;   // Enables the Variable Indicating Sensors Sampling Interval
unsigned char SCI_ACTIVE;       // Used by Timer to control counter when SCI Interrupt Active (Timing Initiative)


/********************              FUNCTIONS                 ******************/
/*
 This Function is created to initialize all the necessary ports of the MC9s12DG256
 MCU to perform the neccessary functions defined in the OSv1 design guide.
*/
void InitializePORTS_DDR()
{                                           
	// Start of initializerPORTS_DDR  
	DisableInterrupts;        // Disables all Maskable Interrupts

	//  USED FOR TESTS ONLY
	DDRJ = 0xFF;             // PJ1 outputs
	PTJ  = 0x00;             // PJ1 = 1 (disables on-board LED's)
	DDRB = 0xFF;             // Sets Port B for output operations

	// Analog to Digital Control Registers (PAD8 - PAD15)
	ATD1CTL2 = 0xC0;   // 1100 0000 -- ADC On = 1, AFFC = 1, ASCIE = 0
	ATD1CTL3 = 0x08;   // 0 0001 000 -- one conversion per sequence
	ATD1CTL4 = 0x00;   // 0 00 01011 -- 10-bit res., 2 A/D clks, 1 MHz conv freq

	// PortH Control Registers (PTH0 - PTH7)
	DDRH = 0xF0;       // PH0-PH3 Inputs - PH7-PH4 Outputs
	PPSH = 0x00;       // Enable pull-up PH0,define FE (Falling Edge) trigger for interrupts
	PERH = 0x0F;       // Enables pull_up or pull_down state
	PIFH = 0x0F;       // Clear port Interrupt flags by sending 1s active edge
	PIEH = 0x0F;       // Enable PH0-4 for port interrupts

	// PWM Control Signals for iMDrive Systems and LED Array (PTP0 - PTP7)
	PWMPRCLK = 0x66;   // Clock A and B Equal 24 MHz / 128 =  187.5kHz
	PWMCLK = 0xFF;     // ClockSA and ClockSB are used for (PTP0 - PTP7) Respectively
	PWMSCLA = 47;      // ClockSA= 187.5 kHz / (2X94) = 500 Hz
	PWMSCLB = 47;      // ClockSA= 187.5 kHz / (2X94) = 500 Hz
	PWMCTL = 0x00;     // PTP0 - PTP7 equal individual 8 Bit PWM Channels
	PWMPOL = 0xFF;     // High Pulse First and goes low when duty is reached
	PWMCAE = 0x00;     // Center Align Output Signal
	//Pulse Width Configuration - CLOCK SA (PTP0-1 and PTP4-5)
	PWMPER0 = 123;     // Has 255 Different Values for Control
	PWMPER1 = 123;     // Has 255 Different Values for Control
	PWMPER4 = 123;     // Has 255 Different Values for Control
	PWMPER5 = 123;     // Has 255 Different Values for Control
	//Pulse Width Configuration - CLOCK SB (PTP2-3 - PTP6-7)
	PWMPER2 = 123;     // Has 255 Different Values for Control
	PWMPER3 = 123;     // Has 255 Different Values for Control
	PWMPER6 = 123;     // Has 255 Different Values for Control
	PWMPER7 = 123;     // Has 255 Different Values for Control
	PWME = 0x00;         // Disables Channel (PTP0 - PTP7)
	   
	// Sensor Sample Timer - Configuration and Start-Up Values
	TSCR1 = 0x80;      // Timer Functions under Normal Operations
	TSCR2 = 0x87;      // TOI Enabled , at 24MHz / 128 Clock Frequency
	ST_1 = 0;          // Sets variable ST_1 to 0 to initialize Sample Timer
				   
	// SCI Input/Output PORT  Control Registers
	//Set Buad Rate>>> BAUD = 24MHz / (16 * BR), BR in BDL and BDH Registers
	DDRS = 0x08;       // PS2 Input and PS3 Output 
	/* The following values are #defines in UROV_Functions.h */
	SCI1BDL = BDL_REGISTER;
	SCI1BDH = BDH_REGISTER;  
	 
	//Set-Up Control Registers
	SCI1CR1 = 0x25;    	// NOP, Enable Wait, Ext Conn., 8 Bit, Idle Line, Stop, PD,OP
	SCI1CR2 = 0x2C;    	// Reciever Full Interrupt Enabled, Enable Transmitter and Reciever
	 
	EnableInterrupts;	// Enables all Maskable Interrupts

}  	 // End of initializerPORTS_DDR                                          


/*
    This Function is created to control the iMDrive Systems for the Bilge Pumps
 used in the system.
*/
void iMDrive_System(void)
{
	// Disables X_Drive and Y_Drive PWM Signals
	if(((Data_Array[5] > 0x7C)&&(Data_Array[5] < 0x83))&&((Data_Array[3] > 0x7C)&&(Data_Array[3] < 0x83))) 
	{ PWME = 0x00; }		// Disables PTP0 - PTP3 , Sets Y_Drive to 0   124 - 131

	// Up(Climb) and Down(Dive) Drive
	// If X_Drive is within range 0x7C<X_Drive<0x83 
	if(!((Data_Array[5] > 0x7C)&&(Data_Array[5] < 0x83))&&((Data_Array[1] & 0x02) == 0x02))
	{
		if(Data_Array[5] < 0x7C)	// Determines Climb Drive Motion
		{  
			PWMDTY4 = 128 - Data_Array[5];	// Drives iMDrive3A in the Forward Direction Progressively
			PWME = 0x10;				// Enables PTP4 and PTP5
		}                       

		if(Data_Array[5] > 0x83)  // Determines Dive Drive Motion
		{	 
			PWMDTY5 = Data_Array[5] - 128;	// Drives iMDrive3B in the Reverse Direction Progressively
			PWME = 0x20;				// Enables PTP4 and PTP5
		}
	}

	// Forward and Reverse Drive
	// If X_Drive is within range 0x7C<X_Drive<0x83 
	if(((Data_Array[3] > 0x7C)&&(Data_Array[3] < 0x83))&&(!((Data_Array[5] > 0x7C)&&(Data_Array[5] < 0x83)))
	&&((Data_Array[1] & 0x02) == 0x00))
	{
		if(Data_Array[5] < 0x7C)	// Determines Forward Drive Motion
		{
			PWMDTY0 = 128 - Data_Array[5];	// Drives iMDrive1A in the Forward Direction Progressively
			PWMDTY2 = 128 - Data_Array[5];	// Drives iMDrive2A in the Forward Direction Progressively
			PWME = 0x05;				// Enables PTP0 and PTP2
		}

		if(Data_Array[5] > 0x83)  // Determines Reverse Drive Motion
		{  
			PWMDTY1 = Data_Array[5] - 128;	// Drives iMDrive 1B in the Reverse Direction Progressively
			PWMDTY3 = Data_Array[5] - 128;	// Drives iMDrive2B in the Reverse Direction Progressively
			PWME = 0x0A;				// Enables PTP1 and PTP3
		}
	}

	// Left and Right Drive
	// If Y_Drive is within range 0x7C<Y_Drive<0x83
	if(((Data_Array[5] > 0x7C)&&(Data_Array[5] < 0x83))&&(!((Data_Array[3] > 0x7C)&&(Data_Array[3]  < 0x83)))
	&&((Data_Array[1] & 0x02) == 0x00))
	{
		if(Data_Array[3] <  0x7C)			// Determines Left Drive Motion
		{
			PWMDTY0 = 128 - Data_Array[3];		// Drives iMDrive1A in the Reverse Direction Progressively
			PWMDTY3 = 128 - Data_Array[3];		// Drives iMDrive2A in the Forward Direction Progressively
			PWME = 0x09;				// Enables PTP0 and PTP3
		}

		if(Data_Array[3] >  0x83) 			// Determines Right Drive Motion
		{
			PWMDTY1 = Data_Array[3] - 128;		// Drives iMDrive 1B in the Forward Direction Progressively
			PWMDTY2 = Data_Array[3] - 128;		// Drives iMDrive2B in the Reverse Direction Progressively
			PWME = 0x06;				// Enables PTP1 and PTP2
		}
	}

	// Forward and Reverse with Left or Right ROV Turn
	// If Y_Drive and X_Drive are both within range 0x7C<Y_Drive<0x83
	if((!((Data_Array[3] > 0x7C)&&(Data_Array[3] < 0x83)))&&(!((Data_Array[5] > 0x7C)&&(Data_Array[5] < 0x83)))
	&&((Data_Array[1] & 0x02) == 0x00))
	{
		if(Data_Array[5] < 0x7C)	// Determines Forward Drive Motion
		{
			if(Data_Array[3] < 0x7C)	// Determines Right Drive Motion
			{
				PWMDTY0 = 128 - Data_Array[5];			// Drives iMDrive1A in the Forward Direction Progressively
				PWMDTY2 = ((128 - Data_Array[5])+(128 - Data_Array[3]));	// Drives iMDrive2A in the Forward + X_ Direction Progressively
				PWME = 0x05;				// Enables PTP0 and PTP2
			}
			
			if(Data_Array[3] > 0x83) 	// Determines Left Drive Motion
			{
				PWMDTY0 = ((128 - Data_Array[5])+(Data_Array[3] - 128));	// Drives iMDrive1A in the Forward + X_ Direction Progressively
				PWMDTY2 = 128 - Data_Array[5];			// Drives iMDrive2A in the Forward Direction Progressively
				PWME = 0x05;				// Enables PTP0 and PTP2
			}

		}


		if(Data_Array[5] > 0x83)  // Determines Reverse Drive Motion
		{
			if(Data_Array[3] < 0x7C)	// Determines Left Drive Motion
			{
				PWMDTY1 = Data_Array[5] - 128;			// Drives iMDrive 1B in the Reverse Direction Progressively
				PWMDTY3 = ((128 - Data_Array[3])+(Data_Array[5] - 128));	// Drives iMDrive2B in the Reverse Direction Progressively
			}
	
			if(Data_Array[3] > 0x83)	// Determines Right Drive Motion
			{
				PWMDTY1 = ((Data_Array[3] - 128)+(Data_Array[5] - 128));	// Drives iMDrive 1B in the Reverse Direction Progressively
				PWMDTY3 = Data_Array[5] - 128;			// Drives iMDrive2B in the Reverse Direction Progressively
			}
			PWME = 0x0A;				// Enables PTP1 and PTP3
		}
	}
}


// Used to Measure Light Intensity (Incident to the Surface and the Turbidty)
void Light_System(void)
{
	// Sample Light Intensity without Turbidity Light Activated

	PTH &= 0xEF;     // Ensures that the LED is OFF Displayed on LDR Sensor
	 
	// Set Conversion to AN12-13 to determine Light Intensity
	ATD1CTL5 = 0x84;
	// Waits for conversion
	while((ATD1STAT0 & 0x80)==0){}
	 
	Data_Array[11] = ATD1DR0L;    // Saves Lower Data Bits 7 - 0
	Data_Array[10] = ATD1DR0H;    // Saves Upper Data Bits 15 - 8 (9 - 8 Effectively)
		 
	// Sample Light Intensity with Turbidity Light Activated
	PTH |= 0x10;     // Turns ON LED Incident on LDR Sensor 
	Delay_us(60000);	// Ensures LED is ON before Sampling is performed, Hold for 16mSec

	// Set Conversion to AN12-13 to determine Light Intensity
	ATD1CTL5 = 0x84;
	//Waits for conversion
	while((ATD1STAT0 & 0x80)==0){}
	 
	Data_Array[9] = ATD1DR0L;     // Saves Lower Data Bits 7 - 0
	Data_Array[8] = ATD1DR0H;     // Saves Upper Data Bits 15 - 8 (9 - 8 Effectively)

	PTH &= 0xEF;     // Turns LED OFF Displayed on LDR Sensor
}

void Temperature_System(void) 
{
	// Measures the Temperature of the +5V Regulator
	// Set Conversion to AN14-15 to determine Internal Temperature
	ATD1CTL5 = 0x86;
	// Waits for conversion
	while((ATD1STAT0 & 0x80)==0){}
	 
	Data_Array[13] = ATD1DR0L;    // Saves Lower Data Bits 7 - 0
	Data_Array[12] = ATD1DR0H;    // Saves Upper Data Bits 15 - 8 (9 - 8 Effectively)

	// Measures the External Temperature of 
	// Set Conversion to AN8-9 to determine External Temperature
	ATD1CTL5 = 0x80;
	// Waits for conversion
	while((ATD1STAT0 & 0x80)==0){}
	 
	Data_Array[15] = ATD1DR0L;    // Saves Lower Data Bits 7 - 0
	Data_Array[14] = ATD1DR0H;    // Saves Upper Data Bits 15 - 8 (9 - 8 Effectively)
	 
	// Temperature Measurement Implementation of OFFSET Value
	// Internal Temperature - Adds an OFFSET Value
	Data_Array[15] = Data_Array[15] + 0x20; 
	 
	// External Temperature - Adds an OFFSET Value
	Data_Array[13] = Data_Array[13] + 0x20;
}


// This function is designed to measure the voltage drop across the
// battery voltage divider circuit used to provide a voltage to determine
// the voltage level based upon the value input to the ADC Port AN11.
void Battery_Monitor_System(void) 
{
	// Measures the Battery Level of the +12V Battery
	// Set Conversion to AN10-11 to determine Battery Level
	ATD1CTL5 = 0x82;
	// Waits for conversion
	while((ATD1STAT0 & 0x80)==0){}
	 
	Data_Array[17] = ATD1DR0L + 0x02;    // Saves Lower Data Bits 7 - 0 (Offset = +2)
	Data_Array[16] = ATD1DR0H;           // Saves Upper Data Bits 15 - 8 (9 - 8 Effectively)
}

// Designed to handle the interrupt operations about PORT H, which is used to 
// detect Water from two seperate water detection sensors.
void PORT_H_Interrupt_Handler(void) 
{
	char DETECT;              // Holds the Value of H Interrupt
  
	DETECT = PIFH;            // Assigns PORTH Conditions
  
	// Only Detects Water Leak Once in Chamber 1 Until System is Reset by powering the Craft Off
	// until chamber is inspected, dried and the chamber is deemed water proof
	if(((DETECT & 0x01) == 0x01)&&((Data_Array[18] & 0x0F) != 0x0F))
	{
		// Alert Users Chamber 1 has a leak
		Data_Array[18] |= 0x0F; // Sets Chamber 1 Flag in UROV Command
            
	} 
	if(((Data_Array[18] & 0x0F) == 0x0F))
	{  
		PIFH |= 0x01;  // Clears Interrupt Flag of Chamber 1
	}   

    // Only Detects Water Leak Once in Chamber 2 Until System is Reset by powering the Craft Off
	// until chamber is inspected, dried and the chamber is deemed water proof
	if(((DETECT & 0x02) == 0x02)&&((Data_Array[18] & 0xF0) != 0xF0))
	{
		// Alert Users Chamber 2 has a leak
		Data_Array[18] |= 0xF0; // Sets Chamber 2 Flag in UROV Command   
 
	} 
	if(((Data_Array[18] & 0xF0) == 0xF0))
	{  
		PIFH |= 0x02;  // Clears Interrupt Flag of Chamber 2
	}   
}

/* FUNCTION: SCI_Transmitter *************************************************/
	/**
 	* @brief  Transmits an Array of data out the SCI1 Port
 	* @param  ptrData: Pointer to 8-bit Data Value, Array or Structure
 	* @retval u8NumBytes: Number of bytes to be transmitted
 	*/
void SCI_Transmitter( U8 * ptrData, U8 u8NumBytes ) 
{
	char u8Count;
	
	for ( u8Count = 0; u8Count < u8NumBytes; u8Count++ ) {
		SCI1DRL = *ptrData++;  
		while((SCI1SR1 & 0x80) == 0){};
		/* May not need this */
		Delay_us(50);
     }  
} 

/* FUNCTION: REG_u16Read ******************************************************/
	/**
 	* @brief  Reads value from Register and returns value
 	* @param  u16RegIdx: Index of the Register to Read
 	* @retval U16 Value
 	*/
U16 REG_u16Read( U16 u16RegIdx )
{
   U16 u16RegValue = 0;
   
   u16RegIdx = u16RegIdx * 2;
   /* May have to watch the ordering of the bytes */   
   u16RegValue = (Data_Array[u16RegIdx]  << 8);
   u16RegValue |= Data_Array[u16RegIdx + 1];
   
   return u16RegValue;
} 
 
/* FUNCTION: REG_vWrite ******************************************************/
	/**
 	* @brief  Writes a value to the Register
 	* @param  u16RegIdx: Index of the Register to Read
	* @param  u16Value: Value to be written
 	* @retval Nothing
 	*/
void REG_vWrite( U16 u16RegIdx, U16 u16Value )
{
   u16RegIdx = u16RegIdx * 2;
   /* Watch the ordering of the bytes */
   Data_Array[u16RegIdx] = (unsigned char)((u16Value >> 8) & 0x00FF);
   Data_Array[u16RegIdx + 1] = (unsigned char)(u16Value & 0x00FF);
}   

void Delay_ms(int delay_ms)                       
{	
	// Start of 'delay_ms(int)' function
    int i,j;                            // Initializes Iteration Variables i, j
	for(i=0; i<delay_ms; i++) {         // Counts from 0 to "del-1" logical 
	    for(j=0; j<4000; j++); 			// Counts from 0 to 3999
	}
}   // End of 'delay_ms(int)' function 	

void Delay_us(int delay_us) 
{
	// Delay in micro seconds(usecs)
                            // Beginning of 6 ms delay
	asm ("ldx delay_us");       // 2 clock cycles loads the value 12 in to register X
	asm ("loop: psha");         // 2 clock cycles
	asm ("pula");               // 3 clock cycles
	asm ("psha");               // 2 clock cycles
	asm ("pula");               // 3 clock cycles
	asm ("psha");               // 2 clock cycles
	asm ("pula");               // 3 clock cycles
	asm ("nop");                // 1 clock cycles

	asm ("dbne x,loop");  // 3 clock cycles
    // Total: 24 Clock Cycles 1us / 42ns = 24 E-Clock Cycles
}   // End of 1us Delay




