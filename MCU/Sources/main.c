// Company Name: Marine Electric 
// Design Engineers: Darrell Antonio Pearson, David Vernon, Cory Todd
// Design Start Date: March 29, 2012
// Application: Operating System for Underwater ROV Version 1.0
// System uses the MC9S12DG256 Freescale Microcontroller
// OS Control Functions:
//                      1. Communications to User PC via SCI Port
//                      2. PWM Controls for iMDrive System
//                      3. Sensory Value Capture and Transmit
//                      4. Controlling Power throughout UROV System


#include <hidef.h> 
#include "derivative.h"
#include "d_modbus.h"			  //MODBUS Task header file - Public Functions
#include "UROV_Functions.h"

void main( void )
{
    // Start of Main Function

	MODBUS_vInitialize();		  // Initialize the MODBUS Task and State Hopper	
		
	Data_Array[18] = 0x00;        // Clears Chamber 1 and 2 Status to No Water Detected
	Sample_Sensors = FALSE;       // Sets Sample Sensors Variable to Disabled until Activated by Timer
	SCI_ACTIVE = FALSE;           // Sets SCI_ACTIVE to False to Activate Sample_Sensors Timer
    
	InitializePORTS_DDR();        // MCU Port Registers Set Up
    
	// Tests Access 
	PORTB = 0x0F;
    Delay_ms(250);
    PORTB = 0x00;
  
	// Tests Access 
	PORTB = 0xF0;
    Delay_ms(250);
    PORTB = 0x00;
    
	// Tests Access 
	PORTB = 0xFF;
    Delay_ms(500);
    PORTB = 0x00;

	while(1)                      // Loop Primary System Forever
	{	     
		iMDrive_System();
		
		MODBUS_vCommState();	  // MODBUS State Hopper - Called once through the loop		
		
		// Updates Sensors Status Every 500mS (unless Specified otherwise)
		if (Sample_Sensors == TRUE) 
		{
			Light_System();                // Run Light System Function
			Temperature_System();          // Run Temperature System Function
			Battery_Monitor_System();      // Run Battery Monitor System Function
			Sample_Sensors = FALSE;        // Reset Sample Timer
		}
	}
}   // End of Main Function
  

// UROV OSv1 Function Definitions

/*----------------------------------------Interrupt Vectors-----------------------------------------*/
#pragma CODE_SEG NON_BANKED 

// Interrupt Vector Reference for Timer Overflow PORT via U8_A - MC9S12DP256
interrupt 16 void TOF_ISR (void)
{   // Start of Port H Handler  
	DisableInterrupts; 			// Disables Interrupts 
  
	TFLG2 = 0x80;
     
    if (SCI_ACTIVE == FALSE)	// Dont UpDate Counter if SCI is Working
	{
		ST_1++;
  
		// Update Sample_Sensors Variable for Turbidity and Temperature Readings
		if(ST_1 >= 5) 
		{  
			Sample_Sensors = TRUE; 
			ST_1 = 0; 
		}
		TFLG2 = 0x80;
	}
     
	EnableInterrupts; 			// Enables Interrupts
}	// End of Port H Handler  

// Interrupt Vector Reference for SCI1 Communications via U8_A - MC9S12DP256
interrupt 21 void SCI_ISR (void)
{   // Start of Port H Handler  
	DisableInterrupts; 			// Disables Interrupts 

	SCI_ACTIVE = TRUE;          // Disables Sample_Sensor Timer Update
		
    while((SCI1SR1 & 0x20) == 0){};  
     
    MODBUS_RxData( SCI1DRL );
       
    SCI_ACTIVE = FALSE;                         // Enables Sample_Sensor Timer Update
  
	EnableInterrupts;                           // Enables Interrupts
}                                             // End of Port H Handler  

// Interrupt Vector Reference for PORT H at PH0 and PH1 via U8_A - MC9S12DP256
interrupt 25 void PortH_ISR (void)
{                                             // Start of Port H Handler  (Chambers 1 and 2)
    DisableInterrupts;        // Disable Interrupts when PIFH is set
  
    PORT_H_Interrupt_Handler();  // Run PORT H Interrupt Operation for Water Detection

    EnableInterrupts;       // Enables Interrupts after PIFH is set 

}                         // End of Port H Handler  (Chambers 1 and 2)
#pragma CODE_SEG DEFAULT


/*--------------------------------------------FUNCTIONS---------------------------------------------*/


