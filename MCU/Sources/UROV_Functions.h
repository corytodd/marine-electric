/*******************************************************************************
*           Copyright (c) 2012  Marine Electronics		                       *
********************************************************************************

           @file 	UROV_Functions.h
		   @author	Darrell Pearson
           @version 1.00
           @date	8/5/2012
           @brief	File provided type definitions and macros for ROV Project

*/		   
/******************************************************************************/
// Completed UROV Declarations, Variables and Functions Header File
/********************           COMPILE FLAGS                ******************/
#ifndef UROV_FUNCTIONS_H
   #define UROV_FUNCTIONS_H

/********************           INCLUDE FILES                ******************/

/********************               ENUMS                    ******************/

/********************              DEFINES                   ******************/
/* 	Defines for the Data_Array[] array and the resulting number
	of MODBUS registers
*/
#define ARRAY_SIZE      20
#define REGISTER_SIZE   10


/* 	The correct formula is:
	SBR = 24MHZ / 16 / Baudrate
    For 38400 Baud SBR = 39 - This agrees with page 455 of Han-Way Haung text */
#define BAUD_RATE       38400
#define BD_VALUE        (24000000 / 16 / BAUD_RATE)   
#define BDL_REGISTER    (U8)(BD_VALUE & 0x00FF)
#define BDH_REGISTER	(U8)((BD_VALUE >> 4) & 0x00FF)	

/********************               MACROS                   ******************/

/********************         TYPE DEFINITIONS               ******************/

/********************        PUBLIC FUNCTION PROTOTYPES      ******************/

/********************          LOCAL VARIABLES               ******************/

/********************          GLOBAL VARIABLES              ******************/
/* 	These are made extern so they can be accessed by other source files that
	include this header file - makes them 'PUBLIC' 
*/
extern unsigned char Data_Array[ARRAY_SIZE];
                   
/*
Data_Array[20] - Descriptions

Array              System
Vector         Functionality
____________________________________________________________________________________________

[0],[1]  - Used to Store Commands for UROV (See Below Text Entitled "UROV Command Reference"

       UROV Command uses 8 Bits to represent specific functions 
       within the system as denoted below.

       Bit - Command State Description
  [8 - 15] - (Reserved for Padding 16 Bit Data Frames per Sensor)*
       7   - (1-Power Save Mode Activated) , (0-Power Save Mode Deactivated) 
       6   - Reserved for USE
       5   - Reserved for USE
       4   - (1-Auto LDR LED Activation) , (0-Manual Control via PC)
       3   - Reserved for USE
       2   - Reserved for USE
       1   - (1-Activate Y Axis Dive and Climb Modes) , (0-Activates Normal Drive Mode)
       0   - (1-Activate Angular Drive Mode) , (0-Activate Normal Drive Mode)
[2]  - Used to Store the X-Axis Drive Displacement (Upper 15-8 Bits)
[3]  - Used to Store the X-Axis Drive Displacement (Lower 7-0 Bits)
[4]  - Used to Store the Y-Axis Drive Displacement (Upper 15-8 Bits)
[5]  - Used to Store the Y-Axis Drive Displacement (Lower 7-0 Bits)  
[6]  - Used to Set the LED Power Output Value      (Upper 15-8 Bits)
[7]  - Used to Set the LED Power Output Value      (Lower 7-0 Bits)
[8]  - Used to Express the Turbidity of the Water (LED Activated)  (Upper 15-8 Bits)
[9]  - Used to Express the Turbidity of the Water (LED Activated) (Lower 7-0 Bits) 
[10] - Used to Express the Turbidity of the Water (LED De-Activated) (Upper 15-8 Bits) (ADC - 84)
[11] - Used to Express the Turbidity of the Water (LED De-Activated)(Lower 7-0 Bits) (ADC - 84)
[12] - Used to Store the Internal (Chamber) Temperature Sensor Reading (Upper 15-8 Bits) (ADC - 86)
[13] - Used to Store the Internal (Chamber) Temperature Sensor Reading (Lower 7-0 Bits) (ADC - 86)
[14] - Used to Store the External (Water) Temperature Sensor Reading (Upper 15-8 Bits) (ADC - 80)
[15] - Used to Store the External (Water) Temperature Sensor Reading (Lower 7-0 Bits)  (ADC - 80)
[16] - Battery Level Voltage Reading (Upper 15-8 Bits) (ADC - 82)
[17] - Battery Level Voltage Reading (Lower 7-0 Bits) (ADC - 82)
[18] - Water Detection (0x0F) - Sensor 1  ,  (0xF0) - Sensor 2
Added by David to make MODBUS register number correct - MODBUS uses 16 bits, and we have 10 values
to monitor/change which equates to 20 bytes....
[19]
*/								
extern unsigned char ST_1;              // Stores Sensor Sample Timer Data 
extern unsigned char Sample_Sensors;    // Enables the Variable Indicating Sensors Sampling Interval
extern unsigned char SCI_ACTIVE;        // Used by Timer to control counter when SCI Interrupt Active (Timing Initiative)


/********************		PUBLIC FUNCTIONS                 ******************/
void InitializePORTS_DDR(void);               // Initializes Data Directions of Registers
void iMDrive_System(void);                    // Control the iMDrive Systems 
void Light_System(void);                      // Samples and Contorl Light Entities 
void Temperature_System(void);                // Samples Internal and External Temperatures
void Battery_Monitor_System(void);            // Samples the potential difference across R3 to monitor battery level
void PORT_H_Interrupt_Handler(void);          // Handles PORT H Interrupt Operations 

/* Added by David Vernon for MODBUS */
void SCI_Transmitter( U8 * ptrData, U8 u8NumBytes );
U16 REG_u16Read(U16 u16RegIdx);
void REG_vWrite(U16 u16RegIdx, U16 u16Value);

/* Added by David Vernon - Moved from LCD source file */
void Delay_ms(int delay_ms);
void Delay_us(int delay_us);

#endif 
